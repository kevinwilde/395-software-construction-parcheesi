from copy import deepcopy
from typing import cast, Dict, Iterator, List, Tuple, Union

from Board import Board
from EnterPiece import EnterPiece
from HomeRowCell import HomeRowCell
from IMove import IMove
from MainRingCell import MainRingCell
from MoveExecutor import MoveExecutor
from MoveHome import MoveHome
from MoveMain import MoveMain
from NestCell import NestCell
from Pawn import Pawn

class RuleEnforcer:
    MAX_DOUBLES = 2

    def __init__(self) -> None:
        self.me = MoveExecutor()


    def isGameOver(self, board: Board) -> bool:
        pawns_in_home = {} # type: Dict[str, int]
        for p in board.pawn_locations:
            if board.isInHome(p):
                if p.color not in pawns_in_home:
                    pawns_in_home[p.color] = 0
                pawns_in_home[p.color] += 1
                if pawns_in_home[p.color] == board.NUM_PAWNS_PER_PLAYER:
                    return True
        return False


    def isLegalTurn(self, color: str, orig_brd: Board, dice: List[int],
                    moves: List[IMove]) -> bool:
        brd = orig_brd
        dice = deepcopy(dice)

        for move in moves:
            if move.pawn.color != color:
                return False

            # Check if move is legal
            if not self._isLegalMove(move, dice, brd):
                return False

            # Move is legal, so consume die for this move...
            dice = move.consumeDice(dice)
            # ...and execute move
            brd, bonus = self.me.executeMove(brd, move)
            if bonus > 0:
                dice.append(bonus)

        if self._isBlockadeMoved(orig_brd, brd):
            return False

        return self._isTurnOver(color, brd, dice, orig_brd)


    def _isLegalMove(self, move: IMove, dice: List[int], board: Board) -> bool:
        if not type(move) in (EnterPiece, MoveMain, MoveHome):
            return False

        # In Home
        if board.isInHome(move.pawn):
            return False

        # In Nest
        if board.isInNest(move.pawn):
            if type(move) is not EnterPiece:
                return False
            if sum(dice) != 5 and 5 not in dice:
                return False
            return self._canEnter(move.pawn, board)

        # In MainRing or HomeRow
        m = None # type: Union[MoveMain, MoveHome]
        loc = None # type: Union[MainRingCell, HomeRowCell]

        if board.isInMainRing(move.pawn):
            if type(move) is not MoveMain:
                return False
            m = cast(MoveMain, move)
            loc = cast(MainRingCell, board.getPawnLocation(m.pawn))
        elif board.isInHomeRow(move.pawn):
            if type(move) is not MoveHome:
                return False
            m = cast(MoveHome, move)
            loc = cast(HomeRowCell, board.getPawnLocation(m.pawn))
        else:
            raise Exception("Impossible to reach")

        if m.distance not in dice:
            return False
        if m.start != loc.num:
            return False
        return self._canMove(m.pawn, m.distance, board)


    def _canEnter(self, pawn: Pawn, board: Board) -> bool:
        if not board.isInNest(pawn):
            return False
        return not board.cellHasBlockade(board.entryCell(pawn.color))


    def _canMove(self, pawn: Pawn, distance: int, board: Board) -> bool:
        if board.isInNest(pawn) or board.isInHome(pawn):
            return False
        cur_cell = board.getPawnLocation(pawn)

        for i in range(1, distance+1):
            cur_cell = cur_cell.next(board, pawn.color)
            if not cur_cell:
                return False
            if board.cellHasBlockade(cur_cell):
                return False

        # If this is a safety cell
        if (board.isSafetyCell(cur_cell)):
            # Check that someone else's pawn is not here
            occupants = board.getOccupants(cur_cell)
            return not (occupants and occupants[0].color != pawn.color)
        return True


    def _isBlockadeMoved(self, orig_brd: Board, final_brd: Board) -> bool:
        """
        Check if two pawns which previously formed a blockade are moved
        together within one turn.
        """
        blockades = []
        for p1 in orig_brd.pawn_locations:
            for p2 in orig_brd.pawn_locations:
                if orig_brd.pawnsAreBlockade(p1, p2):
                    blockades.append((p1,p2))

        for p1 in final_brd.pawn_locations:
            for p2 in final_brd.pawn_locations:
                if ((p1,p2) in blockades
                    and final_brd.pawnsAreBlockade(p1, p2)
                    and orig_brd.getPawnLocation(p1) != final_brd.getPawnLocation(p1)):
                    return True
        return False


    def _isTurnOver(self, color: str, cur_brd: Board, dice: List[int],
                    orig_brd: Board) -> bool:
        """
        Check if any of the following are satisfied:
            1) all dice have been consumed
            2) there are no legal moves remaining
        """
        if sum(dice) == 0:
            return True

        pawns = cur_brd.getPawns(color)
        try:
            # try to get first legal move, which means turn is not over
            _ = next(self.generateLegalMoves(pawns, cur_brd, dice, orig_brd))
            return False
        except StopIteration:
            # no legal move exists
            return True

    def generateLegalMoves(self, pawns: List[Pawn], cur_brd: Board, dice: List[int],
                            orig_brd: Board) -> Iterator[IMove]:
        possible_moves = self.generatePossibleMoves(pawns, cur_brd, dice)
        for m in possible_moves:
            if self._isLegalMove(m, dice, cur_brd):
                brd, _ = self.me.executeMove(cur_brd, m)
                if not self._isBlockadeMoved(orig_brd, brd):
                    yield m

    def generatePossibleMoves(self, pawns: List[Pawn], board: Board,
                              dice: List[int]) -> Iterator[IMove]:
        for pawn in pawns:
            if board.isInNest(pawn):
                if 5 in dice or sum(dice) == 5:
                    yield EnterPiece(pawn)

            elif board.isInMainRing(pawn):
                for d in dice:
                    if d > 0:
                        mrcell = cast(MainRingCell, board.getPawnLocation(pawn))
                        yield MoveMain(pawn, d, mrcell.num)

            elif board.isInHomeRow(pawn):
                for d in dice:
                    if d > 0:
                        hrcell = cast(HomeRowCell, board.getPawnLocation(pawn))
                        yield MoveHome(pawn, d, hrcell.num)
