import sys

from LocalTournament import LocalTournament

num_humans = 0
if len(sys.argv) > 1:
    num_humans = int(sys.argv[1])

t = LocalTournament(num_humans=num_humans)
t.run()
