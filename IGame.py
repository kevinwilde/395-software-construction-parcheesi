from typing import List

from Board import Board
from IPlayer import IPlayer
from SPlayer import SPlayer

class IGame:
    def __init__(self) -> None:
        self.board = None # type: Board
        self.splayers = None # type: List[SPlayer]
        raise NotImplementedError("You can't initialize an interface.")

    def register(self, player: IPlayer) -> bool:
        """
        Add a player to the game.
        """
        raise NotImplementedError("This is an interface method.")

    def start(self) -> None:
        """
        Start a game.
        """
        raise NotImplementedError("This is an interface method.")
