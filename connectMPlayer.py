import sys

from PlayerClient import PlayerClient
from MPlayerHeuristic import MPlayerHeuristic

ip = '127.0.0.1'
port = 8000
if len(sys.argv) > 1:
    ip = sys.argv[1]
if len(sys.argv) > 2:
    port = int(sys.argv[2])

c = PlayerClient(MPlayerHeuristic(), (ip, port))
c.connect()
