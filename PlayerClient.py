import xml.etree.ElementTree as ET
import socket
from typing import Tuple

import ParcheesiXML as PXML
from IPlayer import IPlayer

class PlayerClient:
    def __init__(self, player: IPlayer, server_addr: Tuple[str, int]) -> None:
        self.player = player
        self.server_addr = server_addr

    def connect(self) -> None:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(self.server_addr)

        while True:
            try:
                xml = PXML.recvXML(sock)
            except ET.ParseError:
                sock.close()
                return
            if PXML.XMLIsStartGame(xml):
                resp_xml = self.startGame(xml)
            elif PXML.XMLIsDoMove(xml):
                resp_xml = self.doMove(xml)
            elif PXML.XMLIsDoublesPenalty(xml):
                resp_xml = self.doublesPenalty(xml)
            resp = PXML.tostring(resp_xml) + '\n'
            sock.send(resp.encode('utf-8'))


    def startGame(self, xml: ET.Element) -> ET.Element:
        color = PXML.XMLToStartGame(xml)
        name = self.player.startGame(color)
        return PXML.NameToXML(name)

    def doMove(self, xml: ET.Element) -> ET.Element:
        board, dice = PXML.XMLToDoMove(xml)
        moves = self.player.doMove(board, dice)
        return PXML.MovesToXML(moves)

    def doublesPenalty(self, xml: ET.Element) -> ET.Element:
        return PXML.VoidToXML()
