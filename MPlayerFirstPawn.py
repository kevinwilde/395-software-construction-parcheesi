from typing import List

from Board import Board
from MPlayer import MPlayer
from Pawn import Pawn

class MPlayerFirstPawn(MPlayer):
    def startGame(self, color: str) -> str:
        super().startGame(color)
        return 'MPlayerFirstPawn'

    def _orderPawns(self, board: Board) -> List[Pawn]:
        λ = lambda p: (-board.getPawnLocation(p).distanceFromNest(board, self.color), p.id)
        return board.getPawns(self.color, sortfunc=λ)
