from typing import List, Tuple

from Board import Board
from IMove import IMove
from MoveExecutor import MoveExecutor
from NestCell import NestCell
from Pawn import Pawn

class EnterPiece(IMove):
    def __init__(self, pawn: Pawn) -> None:
        """
        Represents a move where a player enters a piece.
        """
        self.pawn = pawn

    def __eq__(self, other) -> bool:
        return self.pawn == other.pawn

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __repr__(self) -> str:
        return "<EnterPiece pawn=" + str(self.pawn) + ">"

    def execute(self, board: Board, me: MoveExecutor) -> Tuple[Board, int]:
        assert self.pawn in board.pawn_locations
        assert board.getPawnLocation(self.pawn) == NestCell(self.pawn.color)
        board.setPawnLocation(self.pawn, board.entryCell(self.pawn.color))
        if me.bop(board, self.pawn):
            return board, me.BOP_BONUS
        return board, 0

    def consumeDice(self, dice: List[int]) -> List[int]:
        assert sum(dice) > 0
        if 5 in dice:
            dice.pop(dice.index(5))
        else:
            dice = []
        return dice
