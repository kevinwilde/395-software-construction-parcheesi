from copy import deepcopy
from typing import List

from Board import Board
from IMove import IMove
from IPlayer import IPlayer
from MoveExecutor import MoveExecutor
from RuleEnforcer import RuleEnforcer

class Player(IPlayer):
    def __init__(self) -> None:
        self.color = None # type: str
        self.me = MoveExecutor()
        self.re = RuleEnforcer()

    def startGame(self, color: str) -> str:
        """
        Inform the player that a game has started and what color the player is.
        """
        self.color = color
        return 'Player'

    def doMove(self, board: Board, dice: List[int]) -> List[IMove]:
        """
        Ask the player what moves they want to make.
        """
        moves = [] # type: List[IMove]
        orig_brd = deepcopy(board)
        orig_dice = deepcopy(dice)
        while not self.re.isLegalTurn(self.color, orig_brd, orig_dice, moves):
            move = self._chooseMove(board, dice, orig_brd, moves)
            dice = move.consumeDice(dice)
            board, bonus = self.me.executeMove(board, move)
            if bonus > 0:
                dice.append(bonus)
            moves.append(move)
        return moves

    def _chooseMove(self, board: Board, dice: List[int], orig_brd: Board,
                    moves: List[IMove]) -> IMove:
        raise NotImplementedError("This is an abstract function")

    def doublesPenalty(self) -> None:
        """
        Inform the player that they have suffered a doubles penalty.
        """
        pass
