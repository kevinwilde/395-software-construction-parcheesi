from copy import deepcopy
from typing import cast, Dict, List

from Cell import Cell
from HomeCell import HomeCell
from HomeRowCell import HomeRowCell
from MainRingCell import MainRingCell
from NestCell import NestCell
from Pawn import Pawn

class Board:
    NUM_PAWNS_PER_PLAYER = 4
    NUM_PLAYERS = 4
    NUM_CELLS_IN_MAIN_RING = 68
    NUM_CELLS_IN_HOME_ROW = 7
    _EXIT_CELLS = {
        "green": 0,
        "red": 17,
        "blue": 34,
        "yellow": 51
    }
    COLORS = list(_EXIT_CELLS.keys())
    _CELLS_BETWEEN_ENTRY_AND_EXIT = 5
    _SAFETY_CELL_OFFSETS = [0, 5, 12]
    _MAX_OCCUPANTS_PER_CELL = 2

    def __init__(self) -> None:
        self.pawn_locations = {} # type: Dict[Pawn, Cell]

    def __eq__(self, other) -> bool:
        for (k,v) in self.__dict__.items():
            if other.__dict__[k] != v:
                return False
        return True

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def addPawn(self, pawn: Pawn) -> None:
        self.pawn_locations[pawn] = NestCell(pawn.color)

    def getPawnLocation(self, pawn: Pawn) -> Cell:
        assert pawn in self.pawn_locations
        return self.pawn_locations[pawn]

    def setPawnLocation(self, pawn: Pawn, cell: Cell) -> None:
        assert pawn in self.pawn_locations
        self.pawn_locations[pawn] = cell

    def advancePawn(self, pawn: Pawn, distance: int) -> None:
        """
        Move pawn `distance` spaces "ahead" of its current location, where
        "ahead" considers the color of the pawn to determine where it would go.

        PRECONDITION: pawn must be able to move `distance` cells
        """
        assert pawn in self.pawn_locations
        cur_cell = self.getPawnLocation(pawn)
        for _ in range(distance):
            cur_cell = cur_cell.next(self, pawn.color)
            if cur_cell is None:
                raise Exception('Precondition violation: Pawn cannot move {} cells'.format(distance))
        self.setPawnLocation(pawn, cur_cell)

    def isInNest(self, pawn: Pawn) -> bool:
        assert pawn in self.pawn_locations
        return type(self.getPawnLocation(pawn)) is NestCell

    def isInMainRing(self, pawn: Pawn) -> bool:
        assert pawn in self.pawn_locations
        return type(self.getPawnLocation(pawn)) is MainRingCell

    def isInHomeRow(self, pawn: Pawn) -> bool:
        assert pawn in self.pawn_locations
        return type(self.getPawnLocation(pawn)) is HomeRowCell

    def isInHome(self, pawn: Pawn) -> bool:
        assert pawn in self.pawn_locations
        return type(self.getPawnLocation(pawn)) is HomeCell

    def isSafetyCell(self, cell: Cell) -> bool:
        if type(cell) is not MainRingCell:
            return False
        cell = cast(MainRingCell, cell)
        for i in self._EXIT_CELLS.values():
            if cell.num in [i + off for off in self._SAFETY_CELL_OFFSETS]:
                return True
        return False

    def entryCell(self, color: str) -> MainRingCell:
        num = self.exitCell(color).num + self._CELLS_BETWEEN_ENTRY_AND_EXIT
        return MainRingCell(num)

    def exitCell(self, color: str) -> MainRingCell:
        num = self._EXIT_CELLS[color]
        return MainRingCell(num)

    def getOccupants(self, cell: Cell) -> List[Pawn]:
        occupants = []
        for p in self.pawn_locations:
            if self.getPawnLocation(p) == cell:
                occupants.append(p)
        return occupants

    def cellHasBlockade(self, cell: Cell) -> bool:
        if type(cell) in (NestCell, HomeCell):
            return False
        return len(self.getOccupants(cell)) == self._MAX_OCCUPANTS_PER_CELL

    def pawnsAreBlockade(self, p1: Pawn, p2: Pawn) -> bool:
        assert p1 in self.pawn_locations and p2 in self.pawn_locations
        return (p1.id != p2.id
                and p1.color == p2.color
                and self.getPawnLocation(p1) == self.getPawnLocation(p2)
                and self.getPawnLocation(p1) != NestCell(p1.color)
                and self.getPawnLocation(p1) != HomeCell(p1.color))

    def getPawns(self, color: str, sortfunc=lambda p: p.id) -> List[Pawn]:
        """
        Gets pawns of a given color which have not made it home, and sorts
        according to given sort function (default: sort by pawn id)
        """
        pawns = [p for p in self.pawn_locations
                    if p.color == color and not self.isInHome(p)]
        return sorted(pawns, key=sortfunc)

    def farthestPawn(self, color: str) -> Pawn:
        """
        Return the farthest pawn of a given color which has not made it home.
        """
        λ = lambda p: (-self.getPawnLocation(p).distanceFromNest(self, color), p.id)
        pawns = self.getPawns(color, sortfunc=λ)
        if pawns:
            return pawns[0]
        return None
