from typing import List, Union

from Board import Board
from IMove import IMove
from IPlayer import IPlayer

class SPlayer(IPlayer):
    def __init__(self, player: IPlayer, color: str) -> None:
        self.player = player
        self.name = None # type: str
        self.color = color
        self.state = Waiting() # type: Union[Waiting, Started]

    def startGame(self, color: str) -> str:
        assert color in Board.COLORS
        return self.state.startGame(self, color)

    def doMove(self, board: Board, dice: List[int]) -> List[IMove]:
        assert len(dice) == 2 or len(dice) == 4
        return self.state.doMove(self, board, dice)

    def doublesPenalty(self) -> None:
        self.state.doublesPenalty(self)


class Waiting:
    """
    Waiting state for SPlayer
    """
    def startGame(self, sp: SPlayer, color: str) -> str:
        sp.state = Started()
        sp.name = sp.player.startGame(color)
        return sp.name

    def doMove(self, sp: SPlayer, board: Board, dice: List[int]) -> List[IMove]:
        raise ValueError("Cannot doMove from state Waiting")

    def doublesPenalty(self, sp: SPlayer) -> None:
        raise ValueError("Cannot doublesPenalty from state Waiting")


class Started:
    """
    Started state for SPlayer
    """
    def startGame(self, sp: SPlayer, color: str) -> str:
        raise ValueError("Cannot startGame from state Started")

    def doMove(self, sp: SPlayer, board: Board, dice: List[int]) -> List[IMove]:
        moves = None
        while not self._isListOfMoves(moves):
            moves = sp.player.doMove(board, dice)
        return moves

    def doublesPenalty(self, sp: SPlayer) -> None:
        sp.player.doublesPenalty()

    def _isListOfMoves(self, moves) -> bool:
        if not type(moves) is list:
            return False
        for move in moves:
            if not isinstance(move, IMove):
                return False
        return True
