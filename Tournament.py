from random import shuffle
from typing import Dict

from Admin import Admin
from HPlayer import HPlayer
from MPlayerFirstPawn import MPlayerFirstPawn
from MPlayerLastPawn import MPlayerLastPawn
from SPlayer import SPlayer

class Tournament:
    def __init__(self) -> None:
        self.admin = Admin()

    def run(self, num_games: int=1) -> None:
        for i in range(num_games):
            winner = self.playGame()
            print("Game {} won by: {}({})".format(i+1, winner.name, winner.color))

    def playGame(self) -> SPlayer:
        """
        Plays a game and returns the player that won
        """
        self.admin.newGame()
        self.registerPlayers()
        print("Players registered")
        self.admin.ready()
        self.admin.loop()
        color = self.colorOfWinner()
        return self.findPlayerByColor(color)

    def registerPlayers(self) -> None:
        raise NotImplementedError("This is an abstract function")

    def colorOfWinner(self) -> str:
        b = self.admin.game.board
        pawns_in_home = {} # type: Dict[str, int]
        for p in b.pawn_locations:
            if b.isInHome(p):
                if p.color not in pawns_in_home:
                    pawns_in_home[p.color] = 0
                pawns_in_home[p.color] += 1
                if pawns_in_home[p.color] == b.NUM_PAWNS_PER_PLAYER:
                    return p.color
        raise ValueError("No winner yet")

    def findPlayerByColor(self, color: str) -> SPlayer:
        for sp in self.admin.game.splayers:
            if sp.color == color:
                return sp
        raise ValueError("No player found for color " + color)
