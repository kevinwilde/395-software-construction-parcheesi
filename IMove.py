from typing import List, Tuple

from Board import Board
from Pawn import Pawn

class IMove:
    def __init__(self, pawn: Pawn) -> None:
        self.pawn = pawn
        raise NotImplementedError("You can't initialize an interface.")

    def execute(self, board: Board, me) -> Tuple[Board, int]:
        """
        Executes move and returns the resulting board and bonus amount.
        """
        raise NotImplementedError("This is an interface method.")

    def consumeDice(self, dice: List[int]) -> List[int]:
        """
        Consume the die or dice that were used for this move.
        """
        raise NotImplementedError("This is an interface method.")

