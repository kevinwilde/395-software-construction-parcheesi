from socket import socket
from typing import List

import ParcheesiXML as PXML
from Board import Board
from IMove import IMove
from IPlayer import IPlayer

class NPlayer(IPlayer):
    def __init__(self, conn: socket) -> None:
        self.conn = conn

    def startGame(self, color: str) -> str:
        xml = PXML.tostring(PXML.StartGameToXML(color)) + '\n'
        self.conn.send(xml.encode('utf-8'))
        data = PXML.recvXML(self.conn)
        return PXML.XMLToName(data)

    def doMove(self, board: Board, dice: List[int]) -> List[IMove]:
        xml = PXML.tostring(PXML.DoMoveToXML(board, dice)) + '\n'
        self.conn.send(xml.encode('utf-8'))
        data = PXML.recvXML(self.conn)
        return PXML.XMLToMoves(data)

    def doublesPenalty(self) -> None:
        xml = PXML.tostring(PXML.DoublesPenaltyToXML()) + '\n'
        self.conn.send(xml.encode('utf-8'))
        _ = PXML.recvXML(self.conn)
