from Cell import Cell
from HomeRowCell import HomeRowCell

class MainRingCell(Cell):
    def __init__(self, num: int) -> None:
        self.num = num

    def __eq__(self, other) -> bool:
        if type(other) is not MainRingCell:
            return False
        return self.num == other.num

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __repr__(self) -> str:
        return "<MainRingCell " + str(self.num) + ">"

    def next(self, board, color: str) -> Cell:
        if self == board.exitCell(color):
            return HomeRowCell(0, color)
        return MainRingCell((self.num+1) % board.NUM_CELLS_IN_MAIN_RING)

    def distanceFromNest(self, board, color: str) -> int:
        dist = self.num - board.entryCell(color).num + 1
        if self.num < board.entryCell(color).num:
            dist += board.NUM_CELLS_IN_MAIN_RING
        return dist
