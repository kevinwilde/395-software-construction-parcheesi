from typing import List

from Board import Board
from IMove import IMove
from Pawn import Pawn
from Player import Player

class MPlayer(Player):
    def _chooseMove(self, board: Board, dice: List[int], orig_brd: Board,
                    moves: List[IMove]) -> IMove:
        """
        PRECONDITION: There must be a legal move available.
        """
        sorted_pawns = self._orderPawns(board)
        try:
            return next(self.re.generateLegalMoves(sorted_pawns, board, dice, orig_brd))
        except StopIteration:
            raise Exception("Precondition Violation: called _chooseMove when no legal moves available")

    def _orderPawns(self, board: Board) -> List[Pawn]:
        raise NotImplementedError("This is an abstract function")
