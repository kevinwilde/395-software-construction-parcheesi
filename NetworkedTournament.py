import socket
from typing import cast

from IPlayer import IPlayer
from NPlayer import NPlayer
from PlayerClient import PlayerClient
from SPlayer import SPlayer
from Tournament import Tournament

TCP_IP = '127.0.0.1'
TCP_PORT = 8000

class NetworkedTournament(Tournament):
    def run(self, num_games: int=1) -> None:
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((TCP_IP, TCP_PORT))
        self.sock.listen(1)
        super().run(num_games)
        self.sock.close()

    def playGame(self) -> SPlayer:
        winner = super().playGame()
        for sp in self.admin.game.splayers:
            nplayer = cast(NPlayer, sp.player)
            nplayer.conn.close()
        return winner

    def registerPlayers(self) -> None:
        for _ in range(self.admin.game.board.NUM_PLAYERS):
            conn, addr = self.sock.accept()
            nplayer = NPlayer(conn)
            assert self.admin.register(nplayer)

    def client(self, player: IPlayer) -> None:
        c = PlayerClient(player, (TCP_IP, TCP_PORT))
        c.connect()
