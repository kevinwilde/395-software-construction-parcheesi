from typing import List

from Board import Board
from IMove import IMove
from MPlayer import MPlayer

class MPlayerHeuristic(MPlayer):
    def startGame(self, color: str) -> str:
        super().startGame(color)
        return 'racket-6.9-x86_64-macosx.dmg (106M)'

    def _chooseMove(self, board: Board, dice: List[int], orig_brd: Board,
                    moves: List[IMove]) -> IMove:
        pawns = board.getPawns(self.color)
        moves = list(self.re.generateLegalMoves(pawns, board, dice, orig_brd))
        scores = [self._scoreMove(m, board) for m in moves]
        return moves[scores.index(max(scores))]

    def _scoreMove(self, move: IMove, board: Board) -> int:
        BONUS_MULTIPLIER = 50
        new_board, bonus = self.me.executeMove(board, move)
        return self._scoreBoard(new_board) + bonus * BONUS_MULTIPLIER

    def _scoreBoard(self, board: Board) -> int:
        SAFETY_CELL_VALUE = 150
        BLOCKADE_VALUE = 100
        MAX_DISTANCE_MULTIPLIER = 5
        score = 0
        pawns = board.getPawns(self.color)
        locs = []

        for p in pawns:
            loc = board.getPawnLocation(p)
            locs.append(loc)
            if board.isSafetyCell(loc):
                score += SAFETY_CELL_VALUE

            if board.cellHasBlockade(loc):
                if board.isInMainRing(p):
                    score += BLOCKADE_VALUE
                else:
                    score -= BLOCKADE_VALUE

            score += loc.distanceFromNest(board, self.color)

        if pawns:
            dists = [board.getPawnLocation(p).distanceFromNest(board, self.color) for p in pawns]
            score += max(dists) * MAX_DISTANCE_MULTIPLIER

        # Check if you are in danger of being bopped by an opponent
        BOP_DANGER_RANGE = 7
        BOP_DANGER_PENALTY = -150
        for color in board.COLORS:
            if color != self.color:
                pawns = board.getPawns(color)
                for p in pawns:
                    loc = board.getPawnLocation(p)
                    for _ in range(BOP_DANGER_RANGE):
                        loc = loc.next(board, color)
                        if loc in locs and (not board.isSafetyCell(loc) or loc == board.entryCell(color)):
                            score += BOP_DANGER_PENALTY
                        if not loc:
                            break

        return score
