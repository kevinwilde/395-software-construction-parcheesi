from HomeCell import *
"""
Tests for HomeCell
"""

import unittest
from Board import Board

class HomeCellTests(unittest.TestCase):
    def testHomeCellEquality(self):
        c1 = HomeCell("blue")
        c2 = HomeCell("blue")
        self.assertEqual(c1, c2)
        c1 = HomeCell("blue")
        c2 = HomeCell("red")
        self.assertNotEqual(c1, c2)

    def testHomeCellRepr(self):
        c1 = HomeCell("blue")
        self.assertEqual(c1.__repr__(), "<HomeCell blue>")

    def testNext(self):
        b = Board()
        c = HomeCell("blue")
        self.assertIsNone(c.next(b, "blue"))
        # Wrong color
        with self.assertRaises(AssertionError):
            c.next(b, "red")

    def testDistanceFromNest(self):
        b = Board()
        c = HomeCell("blue")
        self.assertEqual(c.distanceFromNest(b, "blue"),
                         b.NUM_CELLS_IN_MAIN_RING + b.NUM_CELLS_IN_HOME_ROW)
