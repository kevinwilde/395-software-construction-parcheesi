from SPlayer import *
"""
Tests for SPlayer
"""

import unittest
from EnterPiece import EnterPiece
from MoveHome import MoveHome
from MoveMain import MoveMain
from Pawn import Pawn
from TPlayer import TPlayer

class SPlayerTests(unittest.TestCase):
    def testStartGame(self):
        sp = SPlayer(TPlayer(), "blue")
        self.assertTrue(isinstance(sp.state, Waiting))
        sp.startGame(sp.color)
        self.assertTrue(isinstance(sp.state, Started))
        with self.assertRaises(ValueError):
            sp.startGame(sp.color)

    def testDoMove(self):
        sp = SPlayer(TPlayer(), "blue")
        with self.assertRaises(ValueError):
            sp.doMove(Board(), [1, 2])
        sp.startGame(sp.color)
        # test that calling doMove doesn't raise an error
        sp.doMove(Board(), [1, 2])
        self.assertTrue(isinstance(sp.state, Started))

    def testDoublesPenalty(self):
        sp = SPlayer(TPlayer(), "blue")
        with self.assertRaises(ValueError):
            sp.doublesPenalty()
        sp.startGame(sp.color)
        # test that calling doMove doesn't raise an error
        sp.doublesPenalty()
        self.assertTrue(isinstance(sp.state, Started))

    def test_isListOfMoves(self):
        s = Started()
        self.assertTrue(s._isListOfMoves([]))
        self.assertTrue(s._isListOfMoves([EnterPiece(Pawn(1, "blue"))]))
        self.assertTrue(s._isListOfMoves([MoveMain(Pawn(1, "blue"), 1, 1)]))
        self.assertTrue(s._isListOfMoves([MoveHome(Pawn(1, "blue"), 1, 1)]))
        self.assertFalse(s._isListOfMoves(None))
        self.assertFalse(s._isListOfMoves(1))
        self.assertFalse(s._isListOfMoves([1]))
