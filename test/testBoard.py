from Board import *
"""
Tests for Board
"""

import unittest

class BoardTests(unittest.TestCase):
    def setUp(self):
        self.b = Board()

    def testBoardEquality(self):
        b1 = Board()
        b2 = Board()
        self.assertTrue(b1 == b2)
        p = Pawn(1, "blue")
        b1.addPawn(p)
        self.assertTrue(b1 != b2)
        b2.addPawn(p)
        self.assertTrue(b1 == b2)

    def testAddPawn(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertTrue(p in self.b.pawn_locations)
        self.assertEqual(self.b.getPawnLocation(p), NestCell(p.color))

    def testgetPawnLocation(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertEqual(self.b.getPawnLocation(p), NestCell(p.color))

    def testsetPawnLocation(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertTrue(p in self.b.pawn_locations)
        self.assertEqual(self.b.getPawnLocation(p), NestCell(p.color))
        self.b.setPawnLocation(p, MainRingCell(4))
        self.assertEqual(self.b.getPawnLocation(p), MainRingCell(4))

    def testAdvancePawn(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertEqual(self.b.getPawnLocation(p), NestCell(p.color))
        # Move forward 1 cell
        self.b.advancePawn(p, 1)
        self.assertEqual(self.b.getPawnLocation(p), self.b.entryCell(p.color))
        self.b.advancePawn(p, 1)
        self.assertEqual(self.b.getPawnLocation(p), MainRingCell(self.b.entryCell(p.color).num+1))
        # Move forward to exit cell
        self.b.setPawnLocation(p, MainRingCell(self.b.exitCell(p.color).num - 3))
        self.b.advancePawn(p, 3)
        self.assertEqual(self.b.getPawnLocation(p), self.b.exitCell(p.color))
        # Move forward into home row
        self.b.advancePawn(p, 1)
        self.assertEqual(self.b.getPawnLocation(p), HomeRowCell(0, p.color))
        self.b.advancePawn(p, 3)
        self.assertEqual(self.b.getPawnLocation(p), HomeRowCell(3, p.color))
        self.b.advancePawn(p, 4)
        self.assertEqual(self.b.getPawnLocation(p), HomeCell(p.color))
        # Precondition violation
        with self.assertRaises(Exception):
            self.b.setPawnLocation(p, HomeRowCell(3, p.color))
            self.b.advancePawn(p, 10)

    def testIsInNest(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertTrue(self.b.isInNest(p))
        self.b.setPawnLocation(p, MainRingCell(4))
        self.assertFalse(self.b.isInNest(p))
        self.b.setPawnLocation(p, HomeRowCell(0, p.color))
        self.assertFalse(self.b.isInNest(p))
        self.b.setPawnLocation(p, HomeCell(p.color))
        self.assertFalse(self.b.isInNest(p))

    def testIsInMainRing(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertFalse(self.b.isInMainRing(p))
        self.b.setPawnLocation(p, MainRingCell(4))
        self.assertTrue(self.b.isInMainRing(p))
        self.b.setPawnLocation(p, HomeRowCell(0, p.color))
        self.assertFalse(self.b.isInMainRing(p))
        self.b.setPawnLocation(p, HomeCell(p.color))
        self.assertFalse(self.b.isInMainRing(p))

    def testIsInHomeRow(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertFalse(self.b.isInHomeRow(p))
        self.b.setPawnLocation(p, MainRingCell(4))
        self.assertFalse(self.b.isInHomeRow(p))
        self.b.setPawnLocation(p, HomeRowCell(0, p.color))
        self.assertTrue(self.b.isInHomeRow(p))
        self.b.setPawnLocation(p, HomeCell(p.color))
        self.assertFalse(self.b.isInHomeRow(p))

    def testIsInHome(self):
        p = Pawn(1, "blue")
        self.b.addPawn(p)
        self.assertFalse(self.b.isInHome(p))
        self.b.setPawnLocation(p, MainRingCell(4))
        self.assertFalse(self.b.isInHome(p))
        self.b.setPawnLocation(p, HomeRowCell(0, p.color))
        self.assertFalse(self.b.isInHome(p))
        self.b.setPawnLocation(p, HomeCell(p.color))
        self.assertTrue(self.b.isInHome(p))

    def testIsSafetyCell(self):
        for i in self.b._EXIT_CELLS.values():
            for off in self.b._SAFETY_CELL_OFFSETS:
                self.assertTrue(self.b.isSafetyCell(MainRingCell(i+off)))
        self.assertFalse(self.b.isSafetyCell(MainRingCell(23)))
        self.assertFalse(self.b.isSafetyCell(MainRingCell(1)))
        self.assertFalse(self.b.isSafetyCell(MainRingCell(58)))
        self.assertFalse(self.b.isSafetyCell(MainRingCell(7)))

    def testGetOccupants(self):
        pb = Pawn(1, "blue")
        pr = Pawn(1, "red")
        py = Pawn(1, "yellow")
        pg = Pawn(1, "green")
        for pawn in [pb,pr,py,pg]:
            self.b.addPawn(pawn)
            self.b.setPawnLocation(pawn, self.b.entryCell(pawn.color))
        self.assertEqual(self.b.getOccupants(self.b.entryCell(pb.color)), [pb])
        self.assertEqual(self.b.getOccupants(self.b.entryCell(pr.color)), [pr])
        self.assertEqual(self.b.getOccupants(self.b.entryCell(py.color)), [py])
        self.assertEqual(self.b.getOccupants(self.b.entryCell(pg.color)), [pg])

        self.b.pawn_locations = {}
        color = "blue"
        p1 = Pawn(1, color)
        p2 = Pawn(2, color)
        p3 = Pawn(3, color)
        self.b.addPawn(p1)
        self.b.addPawn(p2)
        self.b.addPawn(p3)
        self.b.setPawnLocation(p1, HomeRowCell(0, color))
        self.b.setPawnLocation(p2, HomeRowCell(4, color))
        self.b.setPawnLocation(p3, HomeRowCell(4, color))
        self.assertEqual(self.b.getOccupants(HomeRowCell(0, color)), [p1])
        self.assertEqual(set(self.b.getOccupants(HomeRowCell(4, color))), set([p2, p3]))

    def testCellHasBlockade(self):
        color = "blue"
        p1 = Pawn(1, color)
        p2 = Pawn(2, color)
        # Blockade in main ring
        for p in [p1, p2]:
            self.b.addPawn(p)
            self.b.setPawnLocation(p, self.b.entryCell(color))
        self.assertTrue(self.b.cellHasBlockade(self.b.entryCell(color)))
        # Blockade in home row
        for p in [p1, p2]:
            self.b.setPawnLocation(p, HomeRowCell(0, color))
        self.assertTrue(self.b.cellHasBlockade(HomeRowCell(0, color)))
        # Not a blockade at home cell
        for p in [p1, p2]:
            self.b.setPawnLocation(p, HomeCell(color))
        self.assertFalse(self.b.cellHasBlockade(HomeCell(color)))

    def testPawnsAreBlockade(self):
        color = "blue"
        p1 = Pawn(1, color)
        p2 = Pawn(2, color)
        self.b.addPawn(p1)
        self.b.addPawn(p2)

        # Not a blockaade when in NestCell
        self.assertFalse(self.b.pawnsAreBlockade(p1, p2))

        # Blockade in main ring
        self.b.setPawnLocation(p1, MainRingCell(15))
        self.b.setPawnLocation(p2, MainRingCell(15))
        self.assertTrue(self.b.pawnsAreBlockade(p1, p2))

        # Blockade in home row
        self.b.setPawnLocation(p1, HomeRowCell(1, p1.color))
        self.b.setPawnLocation(p2, HomeRowCell(1, p2.color))
        self.assertTrue(self.b.pawnsAreBlockade(p1, p2))

        # Not a blockade in home
        self.b.setPawnLocation(p1, HomeCell(p1.color))
        self.b.setPawnLocation(p2, HomeCell(p2.color))
        self.assertFalse(self.b.pawnsAreBlockade(p1, p2))

        # Not a blockade when at different cells
        self.b.setPawnLocation(p1, MainRingCell(15))
        self.b.setPawnLocation(p2, MainRingCell(16))
        self.assertFalse(self.b.pawnsAreBlockade(p1, p2))

    def testGetPawns(self):
        color = "blue"
        pb0 = Pawn(0, color)
        pb1 = Pawn(1, color)
        pb2 = Pawn(2, color)
        pb3 = Pawn(3, color)
        for p in [pb0, pb1, pb2, pb3]:
            self.b.addPawn(p)
        self.assertEqual(self.b.getPawns(color), [pb0, pb1, pb2, pb3])
        # Sort by distance
        λ = lambda p: (-self.b.getPawnLocation(p).distanceFromNest(self.b, color), p.id)
        self.assertEqual(self.b.getPawns(color, λ), [pb0, pb1, pb2, pb3])
        self.b.setPawnLocation(pb3, self.b.entryCell(color))
        self.assertEqual(self.b.getPawns(color, λ), [pb3, pb0, pb1, pb2])
        self.b.setPawnLocation(pb2, MainRingCell(self.b.entryCell(color).num + 1))
        self.assertEqual(self.b.getPawns(color, λ), [pb2, pb3, pb0, pb1])
        self.b.setPawnLocation(pb1, self.b.exitCell(color))
        self.assertEqual(self.b.getPawns(color, λ), [pb1, pb2, pb3, pb0])
        self.b.setPawnLocation(pb0, HomeRowCell(0, color))
        self.assertEqual(self.b.getPawns(color, λ), [pb0, pb1, pb2, pb3])
        self.b.setPawnLocation(pb3, HomeRowCell(2, color))
        self.assertEqual(self.b.getPawns(color, λ), [pb3, pb0, pb1, pb2])
        self.b.setPawnLocation(pb2, HomeCell(color))
        self.assertEqual(self.b.getPawns(color, λ), [pb3, pb0, pb1])

    def testFarthestPawn(self):
        color = "blue"
        pb1 = Pawn(1, color)
        pb2 = Pawn(2, color)
        self.b.addPawn(pb1)
        self.b.addPawn(pb2)

        # pb1 ahead of pb2
        self.b.setPawnLocation(pb1, MainRingCell(14))
        self.b.setPawnLocation(pb2, MainRingCell(13))
        self.assertEqual(self.b.farthestPawn(color), pb1)

        # pb2 ahead of pb1
        self.b.setPawnLocation(pb1, MainRingCell(13))
        self.b.setPawnLocation(pb2, MainRingCell(14))
        self.assertEqual(self.b.farthestPawn(color), pb2)

        # pb1 in home row while pb2 is in main ring
        self.b.setPawnLocation(pb1, HomeRowCell(0, color))
        self.b.setPawnLocation(pb2, MainRingCell(13))
        self.assertEqual(self.b.farthestPawn(color), pb1)

        # pb2 in home row while pb1 is in main ring
        self.b.setPawnLocation(pb1, MainRingCell(13))
        self.b.setPawnLocation(pb2, HomeRowCell(0, color))
        self.assertEqual(self.b.farthestPawn(color), pb2)

        # ignore pawns in home
        self.b.setPawnLocation(pb1, MainRingCell(13))
        self.b.setPawnLocation(pb2, HomeCell(color))
        self.assertEqual(self.b.farthestPawn(color), pb1)

        # ignore all pawns in home
        self.b.setPawnLocation(pb1, HomeCell(color))
        self.b.setPawnLocation(pb2, HomeCell(color))
        self.assertEqual(self.b.farthestPawn(color), None)

        # ignore other colors
        pr1 = Pawn(1, "red")
        self.b.addPawn(pr1)
        self.b.setPawnLocation(pr1, MainRingCell(15))
        self.b.setPawnLocation(pb1, MainRingCell(14))
        self.b.setPawnLocation(pb2, MainRingCell(13))
        self.assertEqual(self.b.farthestPawn(color), pb1)
