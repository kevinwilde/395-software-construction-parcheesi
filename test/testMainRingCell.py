from MainRingCell import *
"""
Tests for MainRingCell
"""

import unittest
from Board import Board

class MainRingCellTests(unittest.TestCase):
    def testMainRingCellEquality(self):
        c1 = MainRingCell(1)
        c2 = MainRingCell(1)
        self.assertEqual(c1, c2)
        c1 = MainRingCell(1)
        c2 = MainRingCell(2)
        self.assertNotEqual(c1, c2)

    def testMainRingCellRepr(self):
        c1 = MainRingCell(1)
        self.assertEqual(c1.__repr__(), "<MainRingCell 1>")

    def testNext(self):
        b = Board()
        # MainRingCell to MainRingCell
        c = b.entryCell("blue")
        self.assertEqual(c.next(b, "blue"), MainRingCell(b.entryCell("blue").num+1))
        c = MainRingCell(1)
        self.assertEqual(c.next(b, "blue"), MainRingCell(2))
        # MainRingCell to MainRingCell with wrap around
        c = MainRingCell(b.NUM_CELLS_IN_MAIN_RING-1)
        self.assertEqual(c.next(b, "blue"), MainRingCell(0))
        # MainRingCell to HomeRowCell
        c = b.exitCell("blue")
        self.assertEqual(c.next(b, "blue"), HomeRowCell(0, "blue"))

    def testDistanceFromNest(self):
        b = Board()
        c = b.entryCell("blue")
        self.assertEqual(c.distanceFromNest(b, "blue"), 1)
        c = b.exitCell("blue")
        self.assertEqual(c.distanceFromNest(b, "blue"), 64)
        c1 = MainRingCell(b.NUM_CELLS_IN_MAIN_RING-1)
        c2 = MainRingCell(0)
        self.assertEqual(c1.distanceFromNest(b, "blue") + 1, c2.distanceFromNest(b, "blue"))
