from NestCell import *
"""
Tests for NestCell
"""

import unittest
from Board import Board

class NestCellTests(unittest.TestCase):
    def testNestCellEquality(self):
        c1 = NestCell("blue")
        c2 = NestCell("blue")
        self.assertEqual(c1, c2)
        c1 = NestCell("blue")
        c2 = NestCell("red")
        self.assertNotEqual(c1, c2)

    def testNestCellRepr(self):
        c1 = NestCell("blue")
        self.assertEqual(c1.__repr__(), "<NestCell blue>")

    def testNext(self):
        b = Board()
        c = NestCell("blue")
        # NestCell to entryCell (MainRingCell)
        self.assertEqual(c.next(b, "blue"), b.entryCell("blue"))
        # Wrong color
        with self.assertRaises(AssertionError):
            c.next(b, "red")

    def testDistanceFromNest(self):
        b = Board()
        c = NestCell("blue")
        self.assertEqual(c.distanceFromNest(b, "blue"), 0)
