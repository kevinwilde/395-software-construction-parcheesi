"""
Integration Tests
"""

import unittest
from threading import Thread

from LocalTournament import LocalTournament
from NetworkedTournament import NetworkedTournament
from MPlayerFirstPawn import MPlayerFirstPawn

class IntegrationTests(unittest.TestCase):
    def testLocalTournament(self):
        print("\nIntegration Test: Local Tournament")
        t = LocalTournament()
        t.run()
        print("Local Tournament done")

    def testNetworkedTournament(self):
        print("\nIntegration Test: Networked Tournament")
        tourney = NetworkedTournament()
        server = Thread(target=tourney.run)
        server.start()
        client1 = Thread(target=tourney.client, args=(MPlayerFirstPawn(),), daemon=True)
        client1.start()
        client2 = Thread(target=tourney.client, args=(MPlayerFirstPawn(),), daemon=True)
        client2.start()
        client3 = Thread(target=tourney.client, args=(MPlayerFirstPawn(),), daemon=True)
        client3.start()
        client4 = Thread(target=tourney.client, args=(MPlayerFirstPawn(),), daemon=True)
        client4.start()
        server.join()
        print("Networked Tournament done")
