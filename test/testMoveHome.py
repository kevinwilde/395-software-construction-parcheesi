from MoveHome import *
"""
Tests for MoveHome
"""

import unittest
from HomeCell import HomeCell
from MainRingCell import MainRingCell

class MoveHomeTests(unittest.TestCase):
    def testMoveHomeEquality(self):
        m1 = MoveHome(Pawn(0, "blue"), 3, 0)
        m2 = MoveHome(Pawn(0, "blue"), 3, 0)
        self.assertEqual(m1, m2)
        m1 = MoveHome(Pawn(0, "blue"), 3, 0)
        m2 = MoveHome(Pawn(1, "blue"), 3, 0)
        self.assertNotEqual(m1, m2)
        m1 = MoveHome(Pawn(0, "blue"), 3, 0)
        m2 = MoveHome(Pawn(0, "red"), 3, 0)
        self.assertNotEqual(m1, m2)
        m1 = MoveHome(Pawn(0, "blue"), 3, 0)
        m2 = MoveHome(Pawn(0, "blue"), 4, 0)
        self.assertNotEqual(m1, m2)
        m1 = MoveHome(Pawn(0, "blue"), 3, 0)
        m2 = MoveHome(Pawn(0, "blue"), 3, 1)
        self.assertNotEqual(m1, m2)

    def testMoveHomeRepr(self):
        move = MoveHome(Pawn(0, "blue"), 3, 0)
        self.assertEqual(move.__repr__(), "<MoveHome distance=3 start=0 pawn=<Pawn id=0 color=blue>>")

    def testExecute(self):
        b = Board()
        me = MoveExecutor()
        p = Pawn(1, "blue")
        b.addPawn(p)
        b.setPawnLocation(p, HomeRowCell(0, p.color))

        # moving on home row
        move = MoveHome(p, 6, 0)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(p), HomeRowCell(6, p.color))
        self.assertEqual(bonus, 0)

        # moving home (with bonus)
        b.setPawnLocation(p, HomeRowCell(0, p.color))
        move = MoveHome(p, b.NUM_CELLS_IN_HOME_ROW, 0)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(p), HomeCell(p.color))
        self.assertEqual(bonus, 10)
