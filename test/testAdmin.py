from Admin import *
"""
Tests for Admin
"""

import unittest
from MPlayerFirstPawn import MPlayerFirstPawn

class AdminTests(unittest.TestCase):
    def testNewGame(self):
        admin = Admin()
        self.assertTrue(isinstance(admin.state, GameOver))
        admin.newGame() # No error
        self.assertTrue(isinstance(admin.state, Registration))
        with self.assertRaises(ValueError):
            admin.newGame()
        admin.ready()
        self.assertTrue(isinstance(admin.state, InProgress))
        with self.assertRaises(ValueError):
            admin.newGame()

    def testRegister(self):
        admin = Admin()
        p = MPlayerFirstPawn()
        with self.assertRaises(ValueError):
            admin.register(p)
        admin.newGame()
        self.assertTrue(isinstance(admin.state, Registration))
        admin.register(p) # No error
        self.assertEqual(len(admin.game.splayers), 1)
        self.assertTrue(isinstance(admin.state, Registration))
        admin.state = InProgress()
        with self.assertRaises(ValueError):
            admin.register(p)

    def testReady(self):
        admin = Admin()
        with self.assertRaises(ValueError):
            admin.ready()
        admin.newGame()
        admin.register(MPlayerFirstPawn())
        admin.ready() # No error
        self.assertTrue(isinstance(admin.state, InProgress))
        with self.assertRaises(ValueError):
            admin.ready()

    def testLoop(self):
        admin = Admin()
        with self.assertRaises(ValueError):
            admin.loop()
        admin.newGame()
        with self.assertRaises(ValueError):
            admin.loop()
        admin.register(MPlayerFirstPawn())
        with self.assertRaises(ValueError):
            admin.loop()
        admin.ready()
        self.assertTrue(isinstance(admin.state, InProgress))
        admin.loop() # No error
        self.assertTrue(isinstance(admin.state, GameOver))
        with self.assertRaises(ValueError):
            admin.loop()
