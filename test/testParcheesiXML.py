from ParcheesiXML import *
"""
Tests for ParcheesiXML
"""

import unittest
import xml.etree.ElementTree as ET

class ParcheesiXMLTests(unittest.TestCase):
    def testXMLIsStartGame(self):
        xml = ET.Element('start-game')
        self.assertTrue(XMLIsStartGame(xml))
        xml = ET.Element('do-move')
        self.assertFalse(XMLIsStartGame(xml))
        xml = ET.Element('doubles-penalty')
        self.assertFalse(XMLIsStartGame(xml))

    def testXMLIsDoMove(self):
        xml = ET.Element('start-game')
        self.assertFalse(XMLIsDoMove(xml))
        xml = ET.Element('do-move')
        self.assertTrue(XMLIsDoMove(xml))
        xml = ET.Element('doubles-penalty')
        self.assertFalse(XMLIsDoMove(xml))

    def testXMLIsDoublesPenalty(self):
        xml = ET.Element('start-game')
        self.assertFalse(XMLIsDoublesPenalty(xml))
        xml = ET.Element('do-move')
        self.assertFalse(XMLIsDoublesPenalty(xml))
        xml = ET.Element('doubles-penalty')
        self.assertTrue(XMLIsDoublesPenalty(xml))

    def testVoidToXML(self):
        xml = '<void></void>'
        self.assertEqual(tostring(VoidToXML()), xml)

    def testStartGameToXML_XMLToStartGame(self):
        xml = '<start-game>a</start-game>'
        color = 'a'
        self.assertEqual(tostring(StartGameToXML(color)), xml)
        self.assertEqual(XMLToStartGame(fromstring(xml)), color)
        xml = '<start-game>red</start-game>'
        color = 'red'
        self.assertEqual(tostring(StartGameToXML(color)), xml)
        self.assertEqual(XMLToStartGame(fromstring(xml)), color)
        # Both XMLToStartGame and StartGameToXML together
        self.assertEqual(XMLToStartGame(StartGameToXML(color)), color)
        self.assertEqual(tostring(StartGameToXML(XMLToStartGame(fromstring(xml)))), xml)

    def testNameToXML_XMLToName(self):
        xml = '<name>a</name>'
        name = 'a'
        self.assertEqual(tostring(NameToXML(name)), xml)
        self.assertEqual(XMLToName(fromstring(xml)), name)
        xml = '<name>Bob</name>'
        name = 'Bob'
        self.assertEqual(tostring(NameToXML(name)), xml)
        self.assertEqual(XMLToName(fromstring(xml)), name)
        # Both XMLToName and NameToXML together
        self.assertEqual(XMLToName(NameToXML(name)), name)
        self.assertEqual(tostring(NameToXML(XMLToName(fromstring(xml)))), xml)

    def testDoMoveToXML_XMLToDoMove(self):
        xml = ("<do-move>"
                 "<board>"
                   "<start></start>"
                   "<main></main>"
                   "<home-rows></home-rows>"
                   "<home></home>"
                 "</board>"
                 "<dice>"
                   "<die>1</die>"
                   "<die>2</die>"
                 "</dice>"
               "</do-move>")
        brd = Board()
        dice = [1, 2]
        self.assertEqual(tostring(DoMoveToXML(brd, dice)), xml)
        self.assertEqual(XMLToDoMove(fromstring(xml)), (brd, dice))
        # Both XMLToDoMove and DoMoveToXML together
        self.assertEqual(XMLToDoMove(DoMoveToXML(brd, dice)), (brd, dice))
        self.assertEqual(tostring(DoMoveToXML(*XMLToDoMove(fromstring(xml)))), xml)

    def testDoublesPenaltyToXML(self):
        xml = '<doubles-penalty></doubles-penalty>'
        self.assertEqual(tostring(DoublesPenaltyToXML()), xml)

    def testBoardToXML_XMLToBoard(self):
        """
        Note that because the board keeps a dict of pawns to cells, there
        is no guarantee of the order we write the pawns into xml.
        Thus, we can't test that `BoardToXML(b) == xml`.
        However, we can test that `XMLToBoard(xml) == b` and then
        subsequently test that `XMLToBoard(BoardToXML(b)) == b`
        in order to test both XMLToBoard and BoardToXML.
        """
        b = Board()
        b.pawn_locations = {
            Pawn(0, "red"): NestCell("red"),
            Pawn(0, "blue"): NestCell("blue")
        }
        xml = ("<board>"
                 "<start>"
                   "<pawn><color>red</color><id>0</id></pawn>"
                   "<pawn><color>blue</color><id>0</id></pawn>"
                 "</start>"
                 "<main></main>"
                 "<home-rows></home-rows>"
                 "<home></home>"
               "</board>"
               )
        self.assertEqual(XMLToBoard(fromstring(xml)), b)
        self.assertEqual(XMLToBoard(BoardToXML(b)), b)
        xml = ("<board>"
                 "<start>"
                   "<pawn><color>red</color><id>3</id></pawn>"
                   "<pawn><color>red</color><id>2</id></pawn>"
                   "<pawn><color>red</color><id>1</id></pawn>"
                   "<pawn><color>red</color><id>0</id></pawn>"
                   "<pawn><color>green</color><id>1</id></pawn>"
                   "<pawn><color>green</color><id>0</id></pawn>"
                   "<pawn><color>blue</color><id>3</id></pawn>"
                   "<pawn><color>blue</color><id>2</id></pawn>"
                   "<pawn><color>blue</color><id>1</id></pawn>"
                   "<pawn><color>blue</color><id>0</id></pawn>"
                 "</start>"
                 "<main>"
                   "<piece-loc>"
                     "<pawn><color>green</color><id>3</id></pawn>"
                     "<loc>7</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>green</color><id>2</id></pawn>"
                     "<loc>6</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>yellow</color><id>3</id></pawn>"
                     "<loc>4</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>yellow</color><id>2</id></pawn>"
                     "<loc>3</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>yellow</color><id>1</id></pawn>"
                     "<loc>1</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>yellow</color><id>0</id></pawn>"
                     "<loc>1</loc>"
                   "</piece-loc>"
                   "</main>"
                 "<home-rows></home-rows>"
                 "<home></home>"
               "</board>"
               )
        b.pawn_locations = {
            Pawn(3, "red"): NestCell("red"),
            Pawn(2, "red"): NestCell("red"),
            Pawn(1, "red"): NestCell("red"),
            Pawn(0, "red"): NestCell("red"),
            Pawn(1, "green"): NestCell("green"),
            Pawn(0, "green"): NestCell("green"),
            Pawn(3, "blue"): NestCell("blue"),
            Pawn(2, "blue"): NestCell("blue"),
            Pawn(1, "blue"): NestCell("blue"),
            Pawn(0, "blue"): NestCell("blue"),
            Pawn(3, "green"): MainRingCell(7),
            Pawn(2, "green"): MainRingCell(6),
            Pawn(3, "yellow"): MainRingCell(4),
            Pawn(2, "yellow"): MainRingCell(3),
            Pawn(1, "yellow"): MainRingCell(1),
            Pawn(0, "yellow"): MainRingCell(1)
        }
        self.assertEqual(XMLToBoard(fromstring(xml)), b)
        self.assertEqual(XMLToBoard(BoardToXML(b)), b)
        xml = ("<board>"
                 "<start>"
                   "<pawn><color>red</color><id>3</id></pawn>"
                   "<pawn><color>red</color><id>2</id></pawn>"
                   "<pawn><color>red</color><id>1</id></pawn>"
                   "<pawn><color>red</color><id>0</id></pawn>"
                   "<pawn><color>green</color><id>1</id></pawn>"
                   "<pawn><color>green</color><id>0</id></pawn>"
                   "<pawn><color>blue</color><id>3</id></pawn>"
                   "<pawn><color>blue</color><id>2</id></pawn>"
                   "<pawn><color>blue</color><id>1</id></pawn>"
                 "</start>"
                 "<main>"
                   "<piece-loc>"
                     "<pawn><color>green</color><id>3</id></pawn>"
                     "<loc>7</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>green</color><id>2</id></pawn>"
                     "<loc>6</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>yellow</color><id>3</id></pawn>"
                     "<loc>4</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>yellow</color><id>2</id></pawn>"
                     "<loc>3</loc>"
                   "</piece-loc>"
                 "</main>"
                 "<home-rows>"
                   "<piece-loc>"
                     "<pawn><color>blue</color><id>0</id></pawn>"
                     "<loc>4</loc>"
                   "</piece-loc>"
                   "<piece-loc>"
                     "<pawn><color>yellow</color><id>1</id></pawn>"
                     "<loc>1</loc>"
                   "</piece-loc>"
                 "</home-rows>"
                 "<home>"
                   "<pawn><color>red</color><id>0</id></pawn>"
                   "<pawn><color>yellow</color><id>0</id></pawn>"
                 "</home>"
               "</board>"
               )
        b.pawn_locations = {
            Pawn(3, "red"): NestCell("red"),
            Pawn(2, "red"): NestCell("red"),
            Pawn(1, "red"): NestCell("red"),
            Pawn(1, "green"): NestCell("green"),
            Pawn(0, "green"): NestCell("green"),
            Pawn(3, "blue"): NestCell("blue"),
            Pawn(2, "blue"): NestCell("blue"),
            Pawn(1, "blue"): NestCell("blue"),
            Pawn(3, "green"): MainRingCell(7),
            Pawn(2, "green"): MainRingCell(6),
            Pawn(3, "yellow"): MainRingCell(4),
            Pawn(2, "yellow"): MainRingCell(3),
            Pawn(0, "blue"): HomeRowCell(4, "blue"),
            Pawn(1, "yellow"): HomeRowCell(1, "yellow"),
            Pawn(0, "red"): HomeCell("red"),
            Pawn(0, "yellow"): HomeCell("yellow")
        }
        self.assertEqual(XMLToBoard(fromstring(xml)), b)
        self.assertEqual(XMLToBoard(BoardToXML(b)), b)

    def testDiceToXML_XMLToDice(self):
        xml = ("<dice>"
                  "<die>3</die>"
                  "<die>2</die>"
               "</dice>"
               )
        d = [3,2]
        self.assertEqual(tostring(DiceToXML(d)), xml)
        self.assertEqual(XMLToDice(fromstring(xml)), d)
        xml = ("<dice>"
                  "<die>1</die>"
                  "<die>2</die>"
               "</dice>"
               )
        d = [1,2]
        self.assertEqual(tostring(DiceToXML(d)), xml)
        self.assertEqual(XMLToDice(fromstring(xml)), d)
        xml = ("<dice>"
                  "<die>1</die>"
                  "<die>1</die>"
                  "<die>6</die>"
                  "<die>6</die>"
               "</dice>"
               )
        d = [1,1,6,6]
        self.assertEqual(tostring(DiceToXML(d)), xml)
        self.assertEqual(XMLToDice(fromstring(xml)), d)
        # Both XMLToDice and DiceToXML together
        self.assertEqual(XMLToDice(DiceToXML(d)), d)
        self.assertEqual(tostring(DiceToXML(XMLToDice(fromstring(xml)))), xml)

    def testMovesToXML_XMLToMoves(self):
        xml1 = ("<enter-piece>"
                  "<pawn><color>blue</color><id>1</id></pawn>"
                "</enter-piece>"
                )
        m1 = EnterPiece(Pawn(1, "blue"))
        xml2 = ("<move-piece-main>"
                  "<pawn><color>red</color><id>3</id></pawn>"
                  "<start>23</start>"
                  "<distance>5</distance>"
                "</move-piece-main>"
                )
        m2 = MoveMain(Pawn(3, "red"), 5, 23)
        xml3 = ("<move-piece-home>"
                  "<pawn><color>red</color><id>3</id></pawn>"
                  "<start>0</start>"
                  "<distance>5</distance>"
                "</move-piece-home>"
                )
        m3 = MoveHome(Pawn(3, "red"), 5, 0)
        xml = "<moves>"+xml1+xml2+xml3+"</moves>"
        m = [m1, m2, m3]
        self.assertEqual(tostring(MovesToXML(m)), xml)
        self.assertEqual(XMLToMoves(fromstring(xml)), m)
        # Both XMLToMoves and MovesToXML together
        self.assertEqual(XMLToMoves(MovesToXML(m)), m)
        self.assertEqual(tostring(MovesToXML(XMLToMoves(fromstring(xml)))), xml)

    def testMoveToXML_XMLToMove(self):
        xml = ("<enter-piece>"
                 "<pawn><color>blue</color><id>1</id></pawn>"
               "</enter-piece>"
               )
        m = EnterPiece(Pawn(1, "blue"))
        self.assertEqual(tostring(MoveToXML(m)), xml)
        self.assertEqual(XMLToMove(fromstring(xml)), m)
        xml = ("<move-piece-main>"
                 "<pawn><color>red</color><id>3</id></pawn>"
                 "<start>23</start>"
                 "<distance>5</distance>"
               "</move-piece-main>"
               )
        m = MoveMain(Pawn(3, "red"), 5, 23)
        self.assertEqual(tostring(MoveToXML(m)), xml)
        self.assertEqual(XMLToMove(fromstring(xml)), m)
        xml = ("<move-piece-home>"
                 "<pawn><color>red</color><id>3</id></pawn>"
                 "<start>0</start>"
                 "<distance>5</distance>"
               "</move-piece-home>"
               )
        m = MoveHome(Pawn(3, "red"), 5, 0)
        self.assertEqual(tostring(MoveToXML(m)), xml)
        self.assertEqual(XMLToMove(fromstring(xml)), m)
        # Both XMLToMove and MoveToXML together
        self.assertEqual(XMLToMove(MoveToXML(m)), m)
        self.assertEqual(tostring(MoveToXML(XMLToMove(fromstring(xml)))), xml)

    def testPawnLocToXML_XMLToPawnLoc(self):
        xml = ("<piece-loc>"
                 "<pawn><color>blue</color><id>1</id></pawn>"
                 "<loc>4</loc>"
               "</piece-loc>"
               )
        p = Pawn(1, "blue")
        loc = 4
        self.assertEqual(tostring(PawnLocToXML(p, loc)), xml)
        self.assertEqual(XMLToPawnLoc(fromstring(xml)), (p, loc))
        xml = ("<piece-loc>"
                 "<pawn><color>red</color><id>3</id></pawn>"
                 "<loc>65</loc>"
               "</piece-loc>"
               )
        p = Pawn(3, "red")
        loc = 65
        self.assertEqual(tostring(PawnLocToXML(p, loc)), xml)
        self.assertEqual(XMLToPawnLoc(fromstring(xml)), (p, loc))
        # Both XMLToPawnLoc and PawnLocToXML together
        self.assertEqual(XMLToPawnLoc(PawnLocToXML(p, loc)), (p, loc))
        self.assertEqual(tostring(PawnLocToXML(*XMLToPawnLoc(fromstring(xml)))), xml)

    def testPawnToXML_XMLToPawn(self):
        xml = ("<pawn>"
                 "<color>blue</color>"
                 "<id>1</id>"
               "</pawn>"
               )
        p = Pawn(1, "blue")
        self.assertEqual(tostring(PawnToXML(p)), xml)
        self.assertEqual(XMLToPawn(fromstring(xml)), p)
        xml = ("<pawn>"
                 "<color>red</color>"
                 "<id>3</id>"
               "</pawn>"
               )
        p = Pawn(3, "red")
        self.assertEqual(tostring(PawnToXML(p)), xml)
        self.assertEqual(XMLToPawn(fromstring(xml)), p)
        # Both XMLToPawn and PawnToXML together
        self.assertEqual(XMLToPawn(PawnToXML(p)), p)
        self.assertEqual(tostring(PawnToXML(XMLToPawn(fromstring(xml)))), xml)
