from MoveMain import *
"""
Tests for MoveMain
"""

import unittest
from Board import Board
from HomeCell import HomeCell
from HomeRowCell import HomeRowCell
from NestCell import NestCell
from Pawn import Pawn

class MoveMainTests(unittest.TestCase):
    def testMoveMainEquality(self):
        m1 = MoveMain(Pawn(0, "blue"), 3, 0)
        m2 = MoveMain(Pawn(0, "blue"), 3, 0)
        self.assertEqual(m1, m2)
        m1 = MoveMain(Pawn(0, "blue"), 3, 0)
        m2 = MoveMain(Pawn(1, "blue"), 3, 0)
        self.assertNotEqual(m1, m2)
        m1 = MoveMain(Pawn(0, "blue"), 3, 0)
        m2 = MoveMain(Pawn(0, "red"), 3, 0)
        self.assertNotEqual(m1, m2)
        m1 = MoveMain(Pawn(0, "blue"), 3, 0)
        m2 = MoveMain(Pawn(0, "blue"), 4, 0)
        self.assertNotEqual(m1, m2)
        m1 = MoveMain(Pawn(0, "blue"), 3, 0)
        m2 = MoveMain(Pawn(0, "blue"), 3, 1)
        self.assertNotEqual(m1, m2)

    def testMoveMainRepr(self):
        move = MoveMain(Pawn(0, "blue"), 3, 0)
        self.assertEqual(move.__repr__(), "<MoveMain distance=3 start=0 pawn=<Pawn id=0 color=blue>>")

    def testExecute(self):
        b = Board()
        me = MoveExecutor()
        # moving a piece
        pb1 = Pawn(1, "blue")
        b.addPawn(pb1)
        b.setPawnLocation(pb1, MainRingCell(12))
        move = MoveMain(pb1, 3, 12)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(pb1), MainRingCell(15))
        self.assertEqual(bonus, 0)

        # can pass a pawn
        pr1 = Pawn(1, "red")
        b.addPawn(pr1)
        b.setPawnLocation(pr1, MainRingCell(17))
        move = MoveMain(pb1, 5, 15)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(pb1), MainRingCell(20))
        self.assertEqual(bonus, 0)

        # can form a blockade with a friendly pawn
        pb2 = Pawn(2, "blue")
        b.addPawn(pb2)
        b.setPawnLocation(pb2, MainRingCell(25))
        move = MoveMain(pb1, 5, 20)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(pb1), MainRingCell(25))
        self.assertEqual(b.getPawnLocation(pb2), MainRingCell(25))
        self.assertEqual(bonus, 0)

        # bopping a piece and get bonus
        b.setPawnLocation(pr1, MainRingCell(26))
        move = MoveMain(pb1, 1, 25)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(pb1), MainRingCell(26))
        self.assertEqual(b.getPawnLocation(pr1), NestCell(pr1.color))
        self.assertEqual(bonus, 20)

        # moving into home row
        b.setPawnLocation(pb1, MainRingCell((b.exitCell(pb1.color).num - 2) % b.NUM_CELLS_IN_MAIN_RING))
        move = MoveMain(pb1, 5, b.getPawnLocation(pb1).num)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(pb1), HomeRowCell(2, pb1.color))
        self.assertEqual(bonus, 0)

        # move from main ring to home
        b.setPawnLocation(pb1, MainRingCell((b.exitCell(pb1.color).num - 2) % b.NUM_CELLS_IN_MAIN_RING))
        move = MoveMain(pb1, 10, b.getPawnLocation(pb1).num)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(pb1), HomeCell(pb1.color))
        self.assertEqual(bonus, 10)
