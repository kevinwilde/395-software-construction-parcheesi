import os
import subprocess
import unittest

import ParcheesiXML as PXML
from RuleEnforcer import RuleEnforcer

class CompareToRobbyTests(unittest.TestCase):
    XML_TESTS_DIRECTORY = 'test/xml-tests/'
    RE = RuleEnforcer()

    def testAllFiles(self):
        for file in os.listdir(self.XML_TESTS_DIRECTORY):
            self.compareToRobby(file)

    def compareToRobby(self, file):
        color, board, dice, moves = self.parse(self.XML_TESTS_DIRECTORY + file)
        our_answer = self.RE.isLegalTurn(color, board, dice, moves)
        cmd = ['racket', 'robby/valid-move.rkt', self.XML_TESTS_DIRECTORY+file]
        result = subprocess.run(cmd, stdout=subprocess.PIPE)
        robby_answer = 'illegal' not in result.stdout.decode('utf-8')
        try:
            self.assertEqual(our_answer, robby_answer)
        except AssertionError:
            print("Test failed for", file)

    def parse(self, file):
        with open(file) as f:
            contents = f.read()
        color, board, dice, moves = contents.split()
        board = PXML.XMLToBoard(PXML.fromstring(board))
        dice = PXML.XMLToDice(PXML.fromstring(dice))
        moves = PXML.XMLToMoves(PXML.fromstring(moves))
        return color, board, dice, moves
