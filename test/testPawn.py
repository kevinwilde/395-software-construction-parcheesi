from Pawn import *
"""
Tests for Pawn
"""

import unittest

class PawnTests(unittest.TestCase):
    def testValidPawn(self):
        p = Pawn(1, "blue")
        self.assertIsInstance(p, Pawn)

    def testInvalidPawn(self):
        with self.assertRaises(AssertionError):
            Pawn(-1, "blue")
        with self.assertRaises(AssertionError):
            Pawn(7, "blue")
        with self.assertRaises(AssertionError):
            Pawn(1, "chartreuse")

    def testPawnEquality(self):
        p1 = Pawn(1, "blue")
        p2 = Pawn(1, "blue")
        self.assertEqual(p1, p2)
        p1 = Pawn(2, "green")
        p2 = Pawn(2, "green")
        self.assertEqual(p1, p2)
        p1 = Pawn(1, "blue")
        p2 = Pawn(1, "red")
        self.assertNotEqual(p1, p2)
        p1 = Pawn(1, "blue")
        p2 = Pawn(2, "blue")
        self.assertNotEqual(p1, p2)
        p1 = Pawn(1, "blue")

    def testPawnRepr(self):
        p1 = Pawn(1, "blue")
        self.assertEqual(p1.__repr__(), "<Pawn id=1 color=blue>")
