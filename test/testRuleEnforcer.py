from RuleEnforcer import *
"""
Tests for RuleEnforcer
"""

import unittest
from HomeCell import HomeCell
from SPlayer import SPlayer
from TPlayer import TPlayer

class RuleEnforcerTests(unittest.TestCase):
    def setUp(self):
        self.b = Board()
        self.me = MoveExecutor()
        self.re = RuleEnforcer()

    def testIsGameOver(self):
        for i in range(self.b.NUM_PAWNS_PER_PLAYER):
            self.b.addPawn(Pawn(i, "blue"))
            self.b.addPawn(Pawn(i, "red"))

        # Game over and blue wins
        for p in self.b.pawn_locations:
            if p.color == "blue":
                self.b.setPawnLocation(p, HomeCell(p.color))
        self.assertTrue(self.re.isGameOver(self.b))

        # Game not over
        pb1 = Pawn(1, "blue")
        self.b.setPawnLocation(pb1, self.b.entryCell(pb1.color))
        self.assertFalse(self.re.isGameOver(self.b))

        # Game not over
        pr1 = Pawn(1, "red")
        self.b.setPawnLocation(pr1, HomeCell(pr1.color))
        self.assertFalse(self.re.isGameOver(self.b))

        # Game over and red wins
        for p in self.b.pawn_locations:
            if p.color == "red":
                self.b.setPawnLocation(p, HomeCell(p.color))
        self.assertTrue(self.re.isGameOver(self.b))

    def testIsLegalTurn(self):
        # can't move someone else's pawn
        sp = SPlayer(TPlayer(), "blue")
        p1 = Pawn(1, "red")
        self.b.addPawn(p1)
        m_enter1 = EnterPiece(p1)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [2, 3], [m_enter1]))

        # enter a piece
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        p1 = Pawn(1, sp.color)
        self.b.addPawn(p1)
        m_enter1 = EnterPiece(p1)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [2, 3], [m_enter1]))

        # enter two pieces with double 5s
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        p1 = Pawn(1, sp.color)
        p2 = Pawn(2, sp.color)
        for p in [p1, p2]:
            self.b.addPawn(p)
        m_enter1 = EnterPiece(p1)
        m_enter2 = EnterPiece(p2)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [5, 5], [m_enter1, m_enter2]))

        # normal turn entering and then moving 4 cells
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        p1 = Pawn(1, sp.color)
        self.b.addPawn(p1)
        m1 = EnterPiece(p1)
        m2 = MoveMain(p1, 4, self.b.entryCell(p1.color).num)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [5,4], [m1]))
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [5,4], [m1, m2]))

        # cannot move a blockade together with two fours
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        p1 = Pawn(1, sp.color)
        p2 = Pawn(2, sp.color)
        for p in [p1, p2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(p1, MainRingCell(self.b.entryCell(p1.color).num + 1))
        self.b.setPawnLocation(p2, MainRingCell(self.b.entryCell(p2.color).num + 1))
        m1 = MoveMain(p1, 4, self.b.getPawnLocation(p1).num)
        m2 = MoveMain(p2, 4, self.b.getPawnLocation(p2).num)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [4,4], [m1, m2]))

        # can only move one die, since moving both would mean moving a blockade together
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        for p in [pb1, pb2, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(self.b.entryCell(pb1.color).num + 1))
        self.b.setPawnLocation(pb2, MainRingCell(self.b.entryCell(pb2.color).num + 1))
        self.b.setPawnLocation(pr1, MainRingCell(self.b.entryCell(pb1.color).num + 7))
        self.b.setPawnLocation(pr2, MainRingCell(self.b.entryCell(pb2.color).num + 7))
        m1 = MoveMain(pb1, 4, self.b.getPawnLocation(pb1).num)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [4,4], [m1]))

        # cannot move a blockade with two fours and two threes, even if moving 4, 3 with one piece and 3, 4 with the other
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        for p in [pb1, pb2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(self.b.entryCell(pb1.color).num + 1))
        self.b.setPawnLocation(pb2, MainRingCell(self.b.entryCell(pb2.color).num + 1))
        m1 = MoveMain(pb1, 4, self.b.getPawnLocation(pb1).num)
        m2 = MoveMain(pb2, 3, self.b.getPawnLocation(pb2).num)
        m3 = MoveMain(pb1, 3, self.b.getPawnLocation(pb1).num+4)
        m4 = MoveMain(pb2, 4, self.b.getPawnLocation(pb2).num+3)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [4,4,3,3], [m1, m2, m3, m4]))

        # with a blockade and one piece in front of the blockade and a roll of 1,2, it is possible to form a new blockade
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pb3 = Pawn(3, sp.color)
        for p in [pb1, pb2, pb3]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(self.b.entryCell(pb1.color).num + 1))
        self.b.setPawnLocation(pb2, MainRingCell(self.b.entryCell(pb2.color).num + 1))
        self.b.setPawnLocation(pb3, MainRingCell(self.b.entryCell(pb3.color).num + 2))
        m1 = MoveMain(pb3, 1, self.b.getPawnLocation(pb3).num)
        m2 = MoveMain(pb1, 2, self.b.getPawnLocation(pb1).num)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [1,2], [m1, m2]))

        # bop two pieces and move bonuses
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        for p in [pb1, pb2, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, self.b.entryCell(pb1.color))
        self.b.setPawnLocation(pb2, self.b.entryCell(pb2.color))
        self.b.setPawnLocation(pr1, MainRingCell(self.b.entryCell(pb1.color).num + 3))
        self.b.setPawnLocation(pr2, MainRingCell(self.b.entryCell(pb1.color).num + 8))
        m1 = MoveMain(pb1, 3, self.b.getPawnLocation(pb1).num)
        m2 = MoveMain(pb1, 5, self.b.getPawnLocation(pb1).num + 3)
        m3 = MoveMain(pb1, 20, self.b.getPawnLocation(pb1).num + 8)
        m4 = MoveMain(pb2, 20, self.b.getPawnLocation(pb2).num)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [3,5], [m1, m2, m3, m4]))

        # cannot bop twice and then move a blockade together by 20
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pb3 = Pawn(3, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        for p in [pb1, pb2, pb3, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(self.b.entryCell(pb1.color).num + 1))
        self.b.setPawnLocation(pb2, self.b.entryCell(pb2.color))
        self.b.setPawnLocation(pb3, self.b.entryCell(pb3.color))
        self.b.setPawnLocation(pr1, MainRingCell(self.b.entryCell(pb1.color).num + 3))
        self.b.setPawnLocation(pr2, MainRingCell(self.b.entryCell(pb1.color).num + 8))
        m1 = MoveMain(pb1, 2, self.b.getPawnLocation(pb1).num)
        m2 = MoveMain(pb1, 5, self.b.getPawnLocation(pb1).num + 2)
        m3 = MoveMain(pb2, 20, self.b.getPawnLocation(pb2).num)
        m4 = MoveMain(pb3, 20, self.b.getPawnLocation(pb3).num)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [3,5], [m1, m2, m3, m4]))

        # cannot enter home twice and then move a blockade together by 10
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pb3 = Pawn(3, sp.color)
        pb4 = Pawn(0, sp.color)
        for p in [pb1, pb2, pb3, pb4]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, HomeRowCell(self.b.NUM_CELLS_IN_HOME_ROW - 3, pb1.color))
        self.b.setPawnLocation(pb2, HomeRowCell(self.b.NUM_CELLS_IN_HOME_ROW - 4, pb2.color))
        self.b.setPawnLocation(pb3, self.b.entryCell(pb3.color))
        self.b.setPawnLocation(pb4, self.b.entryCell(pb4.color))
        m1 = MoveHome(pb1, 3, self.b.getPawnLocation(pb1).num)
        m2 = MoveHome(pb2, 4, self.b.getPawnLocation(pb2).num)
        m3 = MoveMain(pb3, 10, self.b.getPawnLocation(pb3).num)
        m4 = MoveMain(pb4, 10, self.b.getPawnLocation(pb4).num)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [3, 4], [m1, m2, m3, m4]))

        # cannot ignore die roll
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        p1 = Pawn(1, sp.color)
        p2 = Pawn(2, sp.color)
        for p in [pb1, pb2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(p1, NestCell(p1.color))
        self.b.setPawnLocation(p2, NestCell(p2.color))
        m_enter1 = EnterPiece(p1)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [5, 5], [m_enter1]))

        # cannot move either piece, due to blockade
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        for p in [pb1, pb2, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(14))
        self.b.setPawnLocation(pb2, MainRingCell(15))
        self.b.setPawnLocation(pr1, MainRingCell(16))
        self.b.setPawnLocation(pr2, MainRingCell(16))
        m1 = MoveMain(pb1, 2, self.b.getPawnLocation(pb1).num)
        m2 = MoveMain(pb2, 3, self.b.getPawnLocation(pb2).num)
        self.assertFalse(self.re.isLegalTurn(sp.color, self.b, [2, 3], [m1, m2]))
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [2, 3], []))

        # cannot only take the first die, due to blockade
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        for p in [pb1, pb2, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(13))
        self.b.setPawnLocation(pb2, MainRingCell(15))
        self.b.setPawnLocation(pr1, MainRingCell(16))
        self.b.setPawnLocation(pr2, MainRingCell(16))
        m1 = MoveMain(pb1, 2, self.b.getPawnLocation(pb1).num)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [2, 3], [m1]))

        # cannot only take the second die, due to blockade
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        for p in [pb1, pb2, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(11))
        self.b.setPawnLocation(pb2, MainRingCell(15))
        self.b.setPawnLocation(pr1, MainRingCell(16))
        self.b.setPawnLocation(pr2, MainRingCell(16))
        m1 = MoveMain(pb1, 3, self.b.getPawnLocation(pb1).num)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [2, 3], [m1]))

        # bop, but don't take the 20
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        pr3 = Pawn(3, "red")
        for p in [pb1, pr1, pr2, pr3]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, MainRingCell(15))
        self.b.setPawnLocation(pr1, MainRingCell(18))
        self.b.setPawnLocation(pr2, MainRingCell(20))
        self.b.setPawnLocation(pr3, MainRingCell(20))
        m1 = MoveMain(pb1, 1, self.b.getPawnLocation(pb1).num)
        m2 = MoveMain(pb1, 2, self.b.getPawnLocation(pb1).num + 1)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [1,2], [m1, m2]))

        # move home, but don't take the 10
        self.b.pawn_locations = {}
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        pb2 = Pawn(2, sp.color)
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        for p in [pb1, pb2, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, HomeRowCell(self.b.NUM_CELLS_IN_HOME_ROW - 3, pb1.color))
        self.b.setPawnLocation(pb2, MainRingCell(15))
        self.b.setPawnLocation(pr1, MainRingCell(16))
        self.b.setPawnLocation(pr2, MainRingCell(16))
        m1 = MoveHome(pb1, 3, self.b.getPawnLocation(pb1).num)
        self.assertTrue(self.re.isLegalTurn(sp.color, self.b, [3], [m1]))

    def test_isLegalMove(self):
        # given move must be either EnterPiece, MoveMain, or MoveHome
        self.assertFalse(self.re._isLegalMove(None, [1,3], self.b))

        p = Pawn(1, "blue")
        self.b.addPawn(p)
        m = MoveMain(p, 5, 15)
        # cannot move main before entering main ring
        self.assertFalse(self.re._isLegalMove(m, [2, 4], self.b))

        m_enter = EnterPiece(p)
        # cannot enter with a non-5 roll
        self.assertFalse(self.re._isLegalMove(m_enter, [2, 4], self.b))
        # eneter with [5, *]
        self.assertTrue(self.re._isLegalMove(m_enter, [5, 4], self.b))
        # enter with 2,3
        self.assertTrue(self.re._isLegalMove(m_enter, [3, 2], self.b))
        # enter with 1,4
        self.assertTrue(self.re._isLegalMove(m_enter, [1, 4], self.b))

        # moving a piece
        self.b.setPawnLocation(p, self.b.entryCell(p.color))
        m = MoveMain(p, 2, self.b.entryCell(p.color).num)
        self.assertTrue(self.re._isLegalMove(m, [2, 4], self.b))

        # cannot do a MoveHome when in MainRing
        self.b.setPawnLocation(p, self.b.entryCell(p.color))
        m = MoveHome(p, 4, self.b.getPawnLocation(p).num)
        self.assertFalse(self.re._isLegalMove(m, [4, 5], self.b))

        # cannot do a MoveMain when in HomeRow
        self.b.setPawnLocation(p, HomeRowCell(1, p.color))
        m = MoveMain(p, 5, 1)
        self.assertFalse(self.re._isLegalMove(m, [5], self.b))

        # can move in home row
        self.b.setPawnLocation(p, HomeRowCell(0, p.color))
        m = MoveHome(p, 4, 0)
        self.assertTrue(self.re._isLegalMove(m, [4, 5], self.b))

        # can't overshoot home
        self.b.setPawnLocation(p, HomeRowCell(self.b.NUM_CELLS_IN_HOME_ROW-3, p.color))
        m = MoveHome(p, 4, self.b.NUM_CELLS_IN_HOME_ROW-3)
        self.assertFalse(self.re._isLegalMove(m, [4, 5], self.b))

        # cannot move if no piece present on the square (main ring)
        self.b.setPawnLocation(p, self.b.exitCell(p.color))
        m = MoveMain(p, 5, self.b.getPawnLocation(p).num - 4)
        self.assertFalse(self.re._isLegalMove(m, [5], self.b))

        # cannot move if no piece present on the square (home row)
        self.b.setPawnLocation(p, HomeRowCell(2, p.color))
        m = MoveHome(p, 5, 1)
        self.assertFalse(self.re._isLegalMove(m, [5], self.b))

        # cannot move when already home
        self.b.setPawnLocation(p, HomeCell(p.color))
        m = MoveHome(p, 5, 1)
        self.assertFalse(self.re._isLegalMove(m, [5], self.b))

    def test_canEnter(self):
        pb1 = Pawn(1, "blue")
        pb2 = Pawn(2, "blue")
        pb3 = Pawn(3, "blue")
        for p in [pb1, pb2, pb3]:
            self.b.addPawn(p)
        self.assertTrue(self.re._canEnter(pb1, self.b))
        self.b.setPawnLocation(pb2, self.b.entryCell(pb2.color))
        # cannot enter if not in nest
        self.assertFalse(self.re._canEnter(pb2, self.b))
        self.b.setPawnLocation(pb3, self.b.entryCell(pb3.color))
        # cannot enter with a blockade of your own pawns on the entry point
        self.assertFalse(self.re._canEnter(pb1, self.b))

        # Blue can enter and bop red when red is on blue's entry cell
        self.b.pawn_locations = {}
        pr1 = Pawn(1, "red")
        for p in [pb1, pr1]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb1, NestCell(pb1.color))
        self.b.setPawnLocation(pr1, self.b.entryCell(pb1.color))
        self.assertTrue(self.re._canEnter(pb1, self.b))

        # cannot enter with a blockade on the entry point
        pr2 = Pawn(2, "red")
        self.b.addPawn(pr2)
        self.b.setPawnLocation(pb1, NestCell(pb1.color))
        self.b.setPawnLocation(pr1, self.b.entryCell(pb1.color))
        self.b.setPawnLocation(pr2, self.b.entryCell(pb1.color))
        self.assertFalse(self.re._canEnter(pb1, self.b))

    def test_canMove(self):
        pb1 = Pawn(1, "blue")
        self.b.addPawn(pb1)
        self.assertFalse(self.re._canMove(pb1, 5, self.b))

        self.b.setPawnLocation(pb1, self.b.entryCell(pb1.color))
        self.assertTrue(self.re._canMove(pb1, 5, self.b))

        # _canMoveInMainRing tests
        # can pass a pawn
        self.b.setPawnLocation(pb1, MainRingCell(12))
        pr1 = Pawn(1, "red")
        self.b.addPawn(pr1)
        self.b.setPawnLocation(pr1, MainRingCell(13))
        self.assertTrue(self.re._canMove(pb1, 5, self.b))

        # can form a blockade with a friendly pawn
        pb2 = Pawn(2, "blue")
        self.b.addPawn(pb2)
        self.b.setPawnLocation(pb2, MainRingCell(17))
        self.assertTrue(self.re._canMove(pb1, 5, self.b))

        # cannot add a third piece to blockade
        pb3 = Pawn(3, "blue")
        self.b.addPawn(pb3)
        self.b.setPawnLocation(pb3, MainRingCell(17))
        self.assertFalse(self.re._canMove(pb1, 5, self.b))

        # can bop
        self.b.setPawnLocation(pr1, MainRingCell(13))
        self.assertTrue(self.re._canMove(pb1, 1, self.b))

        # cannot move directly onto an opponent's blockade
        pr2 = Pawn(2, "red")
        self.b.addPawn(pr2)
        self.b.setPawnLocation(pr2, MainRingCell(13))
        self.assertFalse(self.re._canMove(pb1, 1, self.b))

        # cannot pass a blockade of an opponent
        self.assertFalse(self.re._canMove(pb1, 2, self.b))

        # cannot pass one's own blockade
        pr3 = Pawn(3, "red")
        self.b.addPawn(pr3)
        self.b.setPawnLocation(pr3, MainRingCell(11))
        self.assertFalse(self.re._canMove(pr3, 4, self.b))

        # bopping on a safety is illegal
        self.b.pawn_locations = {}
        for p in [pb1, pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pr1, MainRingCell(self.b.entryCell(pb1.color).num-1))
        self.b.setPawnLocation(pb1, self.b.entryCell(pb1.color))
        self.assertTrue(self.b.isSafetyCell(self.b.entryCell(pb1.color)))
        self.assertFalse(self.re._canMove(pr1, 1, self.b))

        # bopping on a safety is illegal, part 2
        self.b.setPawnLocation(pb1, MainRingCell(12))
        self.b.setPawnLocation(pr1, MainRingCell(17))
        self.assertTrue(self.b.isSafetyCell(self.b.getPawnLocation(pr1)))
        self.assertFalse(self.re._canMove(pb1, 5, self.b))

        # can break a blockade
        self.b.setPawnLocation(pr1, MainRingCell(17))
        self.b.setPawnLocation(pr2, MainRingCell(17))
        self.assertTrue(self.re._canMove(pr1, 3, self.b))

        # can move from the main ring to the exit (home) row
        self.b.setPawnLocation(pb1, MainRingCell(self.b.exitCell(pb1.color).num - 3))
        self.assertTrue(self.re._canMove(pb1, 5, self.b))

        # can move from the main ring directly home (without landing on the home row)
        self.b.setPawnLocation(pb1, MainRingCell(self.b.exitCell(pb1.color).num - 3))
        self.assertTrue(self.re._canMove(pb1, self.b.NUM_CELLS_IN_HOME_ROW + 3, self.b))

        # _canMoveInHomeRow tests
        self.b.setPawnLocation(pb1, HomeRowCell(0, pb1.color))
        # can move in home row
        self.assertTrue(self.re._canMove(pb1, 5, self.b))
        # can move to home
        self.assertTrue(self.re._canMove(pb1, self.b.NUM_CELLS_IN_HOME_ROW, self.b))
        # cannot overshoot home
        self.assertFalse(self.re._canMove(pb1, self.b.NUM_CELLS_IN_HOME_ROW + 2, self.b))
        # cannot overshoot home when starting from main ring
        self.b.setPawnLocation(pb1, self.b.exitCell(pb1.color))
        self.assertFalse(self.re._canMove(pb1, 20, self.b))

        # cannot pass a blockade in the home row
        for p in [pb2, pb3]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pb2, HomeRowCell(1, pb2.color))
        self.b.setPawnLocation(pb3, HomeRowCell(1, pb3.color))
        self.assertFalse(self.re._canMove(pb1, 2, self.b))

    def test_isBlockadeMoved(self):
        p1 = Pawn(1, "blue")
        p2 = Pawn(2, "blue")
        for p in [p1, p2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(p1, MainRingCell(14))
        self.b.setPawnLocation(p2, MainRingCell(14))
        m1 = MoveMain(p1, 2, 14)
        final_brd, _ = self.me.executeMove(self.b, m1)
        m2 = MoveMain(p2, 2, 14)
        final_brd, _ = self.me.executeMove(final_brd, m2)
        # Given m1 has been played, m2 would result in blockade moved
        self.assertTrue(self.re._isBlockadeMoved(self.b, final_brd))

        # m2 would not result in blockade moved
        m1 = MoveMain(p1, 2, 14)
        final_brd, _ = self.me.executeMove(self.b, m1)
        m2 = MoveMain(p2, 3, 14)
        final_brd, _ = self.me.executeMove(final_brd, m2)
        self.assertFalse(self.re._isBlockadeMoved(self.b, final_brd))

        # m2 would not result in blockade moved
        self.b.setPawnLocation(p2, NestCell(p2.color))
        m1 = MoveMain(p1, 2, 14)
        final_brd, _ = self.me.executeMove(self.b, m1)
        m2 = EnterPiece(p2)
        final_brd, _ = self.me.executeMove(final_brd, m2)
        self.assertFalse(self.re._isBlockadeMoved(self.b, final_brd))

        # p1 moves 2,4,4 and p2 moves 2. Blockade not moved together
        self.b.setPawnLocation(p1, MainRingCell(14))
        self.b.setPawnLocation(p2, MainRingCell(14))
        m1 = MoveMain(p1, 2, 14)
        final_brd, _ = self.me.executeMove(self.b, m1)
        m2 = MoveMain(p1, 4, 14+2)
        final_brd, _ = self.me.executeMove(final_brd, m2)
        m3 = MoveMain(p1, 4, 14+2+4)
        final_brd, _ = self.me.executeMove(final_brd, m3)
        m4 = MoveMain(p2, 2, 14)
        final_brd, _ = self.me.executeMove(final_brd, m4)
        self.assertFalse(self.re._isBlockadeMoved(self.b, final_brd))

        # p1 moves 2,4 and p2 moves 4,2. Blockade is moved together
        self.b.setPawnLocation(p1, MainRingCell(14))
        self.b.setPawnLocation(p2, MainRingCell(14))
        m1 = MoveMain(p1, 2, 14)
        final_brd, _ = self.me.executeMove(self.b, m1)
        m2 = MoveMain(p1, 4, 14+2)
        final_brd, _ = self.me.executeMove(final_brd, m2)
        m3 = MoveMain(p2, 4, 14)
        final_brd, _ = self.me.executeMove(final_brd, m3)
        m4 = MoveMain(p2, 2, 14+4)
        final_brd, _ = self.me.executeMove(final_brd, m4)
        self.assertTrue(self.re._isBlockadeMoved(self.b, final_brd))

    def test_isTurnOver(self):
        sp = SPlayer(TPlayer(), "blue")
        pb1 = Pawn(1, sp.color)
        self.b.addPawn(pb1)
        # pb1 can enter
        self.assertFalse(self.re._isTurnOver(sp.color, self.b, [1, 4], self.b))
        self.assertFalse(self.re._isTurnOver(sp.color, self.b, [5, 4], self.b))
        # pb1 can't enter
        self.assertTrue(self.re._isTurnOver(sp.color, self.b, [1, 3], self.b))

        pb2 = Pawn(2, sp.color)
        self.b.addPawn(pb2)
        # pb1 and pb2 are initially in a blockade
        self.b.setPawnLocation(pb1, MainRingCell(14))
        self.b.setPawnLocation(pb2, MainRingCell(14))
        self.assertFalse(self.re._isTurnOver(sp.color, self.b, [3, 3], self.b))
        # pb1 moves 3 cells to position 17
        move = MoveMain(pb1, 3, 14)
        cur_board, _ = self.me.executeMove(self.b, move)
        # sp's turn is not over because pb1 can still move 3 cells to position 20
        self.assertFalse(self.re._isTurnOver(sp.color, cur_board, [3], self.b))
        # now, create a blockade in pb1's way
        sp1 = SPlayer(TPlayer(), "red")
        pr1 = Pawn(1, sp1.color)
        pr2 = Pawn(2, sp1.color)
        for p in [pr1, pr2]:
            self.b.addPawn(p)
        self.b.setPawnLocation(pr1, MainRingCell(18))
        self.b.setPawnLocation(pr2, MainRingCell(18))
        # pb1 moves 3 cells to position 17
        move = MoveMain(pb1, 3, 14)
        cur_board, _ = self.me.executeMove(self.b, move)
        # sp's turn is over because pb1 is blocked and pb2 cannot move
        # because this would move a blockade together
        self.assertTrue(self.re._isTurnOver(sp.color, cur_board, [3], self.b))

        # if sp has another piece that can move, turn is not over
        pb3 = Pawn(3, sp.color)
        self.b.addPawn(pb3)
        self.b.setPawnLocation(pb3, HomeRowCell(0, pb3.color))
        # pb1 moves 3 cells to position 17
        move = MoveMain(pb1, 3, 14)
        cur_board, _ = self.me.executeMove(self.b, move)
        self.assertFalse(self.re._isTurnOver(sp.color, cur_board, [3], self.b))

    def test_generatePossibleMoves(self):
        pb0 = Pawn(0, "blue")
        self.b.addPawn(pb0)

        # Entering
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [1])), [])
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [5])), [
            EnterPiece(pb0)
        ])
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [2,3])), [
            EnterPiece(pb0)
        ])

        # Moves in Main Ring
        self.b.setPawnLocation(pb0, MainRingCell(0))
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [1])), [
            MoveMain(pb0, 1, 0)
        ])
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [2,6])), [
            MoveMain(pb0, 2, 0),
            MoveMain(pb0, 6, 0)
        ])

        # Moves in HomeRow
        self.b.setPawnLocation(pb0, HomeRowCell(0, pb0.color))
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [1])), [
            MoveMain(pb0, 1, 0)
        ])
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [2,6])), [
            MoveMain(pb0, 2, 0),
            MoveMain(pb0, 6, 0)
        ])

        # No moves when home
        self.b.setPawnLocation(pb0, HomeCell(pb0.color))
        self.assertEqual(list(self.re.generatePossibleMoves([pb0], self.b, [1])), [])

