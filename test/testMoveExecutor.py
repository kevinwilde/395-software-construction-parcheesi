from MoveExecutor import *
"""
Tests for MoveExecutor
"""

import unittest
from HomeCell import HomeCell
from HomeRowCell import HomeRowCell
from MainRingCell import MainRingCell
from MoveMain import MoveMain

class MoveExecutorTests(unittest.TestCase):
    def setUp(self):
        self.me = MoveExecutor()
        self.b = Board()

    def testExecuteMove(self):
        pb1 = Pawn(1, "blue")
        self.b.addPawn(pb1)
        self.b.setPawnLocation(pb1, MainRingCell(12))
        move = MoveMain(pb1, 3, 12)
        self.b, bonus = self.me.executeMove(self.b, move)
        self.assertEqual(self.b.getPawnLocation(pb1), MainRingCell(15))
        self.assertEqual(bonus, 0)

    def testBop(self):
        pb1 = Pawn(1, "blue")
        pr1 = Pawn(1, "red")
        self.b.addPawn(pb1)
        self.b.addPawn(pr1)
        self.b.setPawnLocation(pb1, MainRingCell(14))
        self.b.setPawnLocation(pr1, MainRingCell(15))

        self.assertFalse(self.me.bop(self.b, pb1))
        self.b.setPawnLocation(pb1, MainRingCell(15))
        self.assertEqual(self.b.getPawnLocation(pr1), MainRingCell(15))
        self.assertTrue(self.me.bop(self.b, pb1))
        self.assertEqual(self.b.getPawnLocation(pr1), NestCell(pr1.color))
        self.assertEqual(self.b.getOccupants(MainRingCell(15)), [pb1])

        # enter a piece and bop an opponent on the safety
        self.b.setPawnLocation(pb1, self.b.entryCell(pb1.color))
        self.b.setPawnLocation(pr1, self.b.entryCell(pb1.color))
        self.assertTrue(self.me.bop(self.b, pb1))
        self.assertEqual(self.b.getPawnLocation(pr1), NestCell(pr1.color))
        self.assertEqual(self.b.getOccupants(self.b.entryCell(pb1.color)), [pb1])

    def test_doublesPenalty(self):
        # simple example, two pawns on board with one in front of the other
        pb1 = Pawn(1, "blue")
        pb2 = Pawn(2, "blue")
        self.b.addPawn(pb1)
        self.b.addPawn(pb2)
        self.b.setPawnLocation(pb1, MainRingCell(14))
        self.b.setPawnLocation(pb2, MainRingCell(13))
        self.me.doublesPenalty(self.b, pb1.color)
        self.assertEqual(self.b.getPawnLocation(pb1), NestCell(pb1.color))
        self.assertEqual(self.b.getPawnLocation(pb2), MainRingCell(13))

        # no pawns in the main ring
        self.b.setPawnLocation(pb1, NestCell(pb1.color))
        self.b.setPawnLocation(pb2, NestCell(pb2.color))
        self.me.doublesPenalty(self.b, pb1.color)
        self.assertEqual(self.b.getPawnLocation(pb1), NestCell(pb1.color))
        self.assertEqual(self.b.getPawnLocation(pb2), NestCell(pb2.color))

        # one pawn in home
        self.b.setPawnLocation(pb1, HomeCell(pb1.color))
        self.b.setPawnLocation(pb2, MainRingCell(13))
        self.me.doublesPenalty(self.b, pb1.color)
        self.assertEqual(self.b.getPawnLocation(pb1), HomeCell(pb1.color))
        self.assertEqual(self.b.getPawnLocation(pb2), NestCell(pb2.color))

        # pawn in home row gets moved back
        self.b.setPawnLocation(pb1, HomeRowCell(1, pb1.color))
        self.b.setPawnLocation(pb2, MainRingCell(13))
        self.me.doublesPenalty(self.b, pb1.color)
        self.assertEqual(self.b.getPawnLocation(pb1), NestCell(pb1.color))
        self.assertEqual(self.b.getPawnLocation(pb2), MainRingCell(13))
