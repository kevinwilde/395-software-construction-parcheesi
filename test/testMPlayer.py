from MPlayer import *
"""
Tests for MPlayer

Note: Tests for doMove are in subclasses that implement _orderPawns
"""

import unittest
from HomeCell import HomeCell

class MPlayerTests(unittest.TestCase):
    def testStartGame(self):
        mp = MPlayer()
        mp.startGame("blue")
        self.assertEqual(mp.color, "blue")
