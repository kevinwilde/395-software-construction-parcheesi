from Game import *
"""
Tests for Game
"""

import unittest
from TPlayer import TPlayer

class GameTests(unittest.TestCase):
    def setUp(self):
        self.g = Game(Board())

    def testRegister(self):
        self.assertTrue(isinstance(self.g.state, Registration))
        for i in range(self.g.board.NUM_PLAYERS):
            self.assertTrue(self.g.register(TPlayer()))
            self.assertEqual(len(self.g.splayers), i+1)

        self.assertEqual(len(self.g.splayers), self.g.board.NUM_PLAYERS)
        self.assertFalse(self.g.register(TPlayer()))
        self.assertEqual(len(self.g.splayers), self.g.board.NUM_PLAYERS)
        self.g.start()
        with self.assertRaises(ValueError):
            self.g.register(TPlayer())

    def testStart(self):
        # self.g is in Registration
        self.g.splayers = [
                            SPlayer(TPlayer(), "blue"),
                            SPlayer(TPlayer(), "red"),
                            SPlayer(TPlayer(), "green"),
                            SPlayer(TPlayer(), "yellow"),
                          ]
        self.assertEqual(len(self.g.board.pawn_locations), 0)
        self.g.start()
        self.assertTrue(isinstance(self.g.state, InProgress))
        self.assertEqual(len(self.g.board.pawn_locations),
                         len(self.g.splayers)*self.g.board.NUM_PAWNS_PER_PLAYER)
        for sp in self.g.splayers:
            pawns = [p for p in self.g.board.pawn_locations if p.color == sp.color]
            self.assertEqual(len(pawns), self.g.board.NUM_PAWNS_PER_PLAYER)
        with self.assertRaises(ValueError):
            self.g.start()
