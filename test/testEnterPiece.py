from EnterPiece import *
"""
Tests for EnterPiece
"""

import unittest

class EnterPieceTests(unittest.TestCase):
    def testEnterPieceEquality(self):
        m1 = EnterPiece(Pawn(0, "blue"))
        m2 = EnterPiece(Pawn(0, "blue"))
        self.assertEqual(m1, m2)
        m1 = EnterPiece(Pawn(0, "blue"))
        m2 = EnterPiece(Pawn(1, "blue"))
        self.assertNotEqual(m1, m2)
        m1 = EnterPiece(Pawn(0, "blue"))
        m2 = EnterPiece(Pawn(0, "red"))
        self.assertNotEqual(m1, m2)

    def testEnterPieceRepr(self):
        move = EnterPiece(Pawn(0, "blue"))
        self.assertEqual(move.__repr__(), "<EnterPiece pawn=<Pawn id=0 color=blue>>")

    def testExecute(self):
        b = Board()
        me = MoveExecutor()
        p = Pawn(1, "blue")
        b.addPawn(p)
        self.assertEqual(b.getPawnLocation(p), NestCell(p.color))
        move = EnterPiece(p)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(p), b.entryCell(p.color))
        self.assertEqual(bonus, 0)

        # enter a piece and bop an opponent on the safety
        b.pawn_locations = {}
        pb1 = Pawn(1, "blue")
        pr1 = Pawn(1, "red")
        b.addPawn(pb1)
        b.addPawn(pr1)
        b.setPawnLocation(pr1, b.entryCell(pb1.color))
        move = EnterPiece(pb1)
        b, bonus = move.execute(b, me)
        self.assertEqual(b.getPawnLocation(pb1), b.entryCell(pb1.color))
        self.assertEqual(b.getPawnLocation(pr1), NestCell(pr1.color))
        self.assertEqual(bonus, 20)
