from HomeRowCell import *
"""
Tests for HomeRowCell
"""

import unittest
from Board import Board

class HomeRowCellTests(unittest.TestCase):
    def testHomeCellEquality(self):
        c1 = HomeRowCell(0, "blue")
        c2 = HomeRowCell(0, "blue")
        self.assertEqual(c1, c2)
        c1 = HomeRowCell(0, "blue")
        c2 = HomeRowCell(0, "red")
        self.assertNotEqual(c1, c2)
        c1 = HomeRowCell(0, "blue")
        c2 = HomeRowCell(1, "blue")
        self.assertNotEqual(c1, c2)

    def testHomeCellRepr(self):
        c1 = HomeRowCell(0, "blue")
        self.assertEqual(c1.__repr__(), "<HomeRowCell 0 blue>")

    def testNext(self):
        b = Board()
        # HomeRowCell to HomeRowCell
        c = HomeRowCell(0, "blue")
        self.assertEqual(c.next(b, "blue"), HomeRowCell(1, "blue"))
        # HomeRowCell to HomeCell
        c = HomeRowCell(b.NUM_CELLS_IN_HOME_ROW-1, "blue")
        self.assertEqual(c.next(b, "blue"), HomeCell("blue"))
        # Wrong color
        with self.assertRaises(AssertionError):
            c.next(b, "red")

    def testDistanceFromNest(self):
        b = Board()
        c = HomeRowCell(0, "blue")
        self.assertEqual(c.distanceFromNest(b, "blue"), b.NUM_CELLS_IN_MAIN_RING)
        c = HomeRowCell(6, "blue")
        self.assertEqual(c.distanceFromNest(b, "blue"), b.NUM_CELLS_IN_MAIN_RING + 6)
