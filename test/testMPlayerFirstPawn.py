from MPlayerFirstPawn import *
"""
Tests for MPlayerFirstPawn
"""

import unittest
from EnterPiece import EnterPiece
from HomeRowCell import HomeRowCell
from MainRingCell import MainRingCell
from MoveHome import MoveHome
from MoveMain import MoveMain
from NestCell import NestCell

class MPlayerFirstPawnTests(unittest.TestCase):
    def _resetPawns(self, board):
        for p in board.pawn_locations:
            board.pawn_locations[p] = NestCell(p.color)

    def testDoMove(self):
        mplayer = MPlayerFirstPawn()
        mplayer.startGame("blue")
        board = Board()
        pb0 = Pawn(0, "blue")
        pb1 = Pawn(1, "blue")
        pb2 = Pawn(2, "blue")
        pb3 = Pawn(3, "blue")
        pr0 = Pawn(0, "red")
        pr1 = Pawn(1, "red")
        pr2 = Pawn(2, "red")
        pr3 = Pawn(3, "red")
        for p in [pb0, pb1, pb2, pb3, pr0, pr1, pr2, pr3]:
            board.addPawn(p)
        dice = [1, 2]
        self.assertEqual(mplayer.doMove(board, dice), [])

        self._resetPawns(board)
        dice = [5]
        self.assertEqual(mplayer.doMove(board, dice), [EnterPiece(pb0)])

        self._resetPawns(board)
        dice = [5, 3]
        self.assertEqual(mplayer.doMove(board, dice), [
            EnterPiece(pb0),
            MoveMain(pb0, 3,board.entryCell("blue").num)
        ])

        self._resetPawns(board)
        dice = [5, 5, 2, 2]
        self.assertEqual(mplayer.doMove(board, dice), [
            EnterPiece(pb0),
            MoveMain(pb0, 5, board.entryCell("blue").num),
            MoveMain(pb0, 2, board.entryCell("blue").num + 5),
            MoveMain(pb0, 2, board.entryCell("blue").num + 7)
        ])

        self._resetPawns(board)
        board.setPawnLocation(pb0, MainRingCell(23))
        dice = [3, 4]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 3, board.getPawnLocation(pb0).num),
            MoveMain(pb0, 4, board.getPawnLocation(pb0).num + 3)
        ])

        self._resetPawns(board)
        board.setPawnLocation(pb0, board.exitCell(pb0.color))
        dice = [1, 2]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 1, board.getPawnLocation(pb0).num),
            MoveHome(pb0, 2, 0)
        ])

        self._resetPawns(board)
        board.setPawnLocation(pb0, board.exitCell(pb0.color))
        dice = [3, 4]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 3, board.getPawnLocation(pb0).num),
            MoveHome(pb0, 4, 2)
        ])

        # because 3 is the first die, it gets used in a move
        # other pawns can't enter on a 6, and the farthest pawn can't move 6
        # because it would overshoot
        self._resetPawns(board)
        board.setPawnLocation(pb0, board.exitCell(pb0.color))
        dice = [3, 6]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 3, board.getPawnLocation(pb0).num)
        ])

        self._resetPawns(board)
        board.setPawnLocation(pb0, board.exitCell(pb0.color))
        dice = [4, 5]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 4, board.getPawnLocation(pb0).num),
            EnterPiece(pb1)
        ])

        # red pawns form a blockade, so no blue pawns can move
        self._resetPawns(board)
        board.setPawnLocation(pr0, MainRingCell(24))
        board.setPawnLocation(pr1, MainRingCell(24))
        board.setPawnLocation(pb0, MainRingCell(22))
        board.setPawnLocation(pb1, MainRingCell(21))
        dice = [3, 4]
        self.assertEqual(mplayer.doMove(board, dice), [])

        # only pb1 can move
        self._resetPawns(board)
        board.setPawnLocation(pr0, MainRingCell(24))
        board.setPawnLocation(pr1, MainRingCell(24))
        board.setPawnLocation(pb0, MainRingCell(22))
        board.setPawnLocation(pb1, MainRingCell(21))
        dice = [2, 4]
        self.assertEqual(mplayer.doMove(board, dice), [MoveMain(pb1, 2, 21)])

        # pb1 can move and pb2 can enter
        self._resetPawns(board)
        board.setPawnLocation(pr0, MainRingCell(24))
        board.setPawnLocation(pr1, MainRingCell(24))
        board.setPawnLocation(pb0, MainRingCell(22))
        board.setPawnLocation(pb1, MainRingCell(21))
        dice = [2, 5]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb1, 2, 21),
            EnterPiece(pb2)
        ])

        # pb2 can enter, then move three
        self._resetPawns(board)
        board.setPawnLocation(pr0, MainRingCell(24))
        board.setPawnLocation(pr1, MainRingCell(24))
        board.setPawnLocation(pb0, MainRingCell(22))
        board.setPawnLocation(pb1, MainRingCell(21))
        dice = [3, 5]
        self.assertEqual(mplayer.doMove(board, dice), [
            EnterPiece(pb2),
            MoveMain(pb2, 3, board.entryCell(pb2.color).num)
        ])

        # pb0 can bop, then pb1 takes the bonus because pb0 can't or it would
        # overshoot home
        self._resetPawns(board)
        board.setPawnLocation(pr0, MainRingCell(24))
        board.setPawnLocation(pr1, MainRingCell(25))
        board.setPawnLocation(pb0, MainRingCell(22))
        board.setPawnLocation(pb1, MainRingCell(21))
        dice = [3]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 3, 22),
            MoveMain(pb1, 20, 21)
        ])

        # pb0 can't bop because pr1 is on a safety cell, so pb1 is moved instead
        self._resetPawns(board)
        board.setPawnLocation(pr1, MainRingCell(0))
        self.assertTrue(board.isSafetyCell(board.getPawnLocation(pr1)))
        board.setPawnLocation(pb0, MainRingCell(67))
        board.setPawnLocation(pb1, MainRingCell(60))
        dice = [1]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb1, 1, 60)
        ])

        # pb0 can pass pr1 even if pr1 is in a safety cell
        self._resetPawns(board)
        board.setPawnLocation(pr1, MainRingCell(0))
        board.setPawnLocation(pb0, MainRingCell(67))
        board.setPawnLocation(pb1, MainRingCell(60))
        dice = [2]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 2, 67)
        ])

        # pb0 can bop, then takes the bonus
        self._resetPawns(board)
        board.setPawnLocation(pb0, MainRingCell(0))
        board.setPawnLocation(pb1, MainRingCell(60))
        board.setPawnLocation(pr1, MainRingCell(1))
        dice = [1]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 1, 0),
            MoveMain(pb0, 20, 1)
        ])

        # bop streak
        self._resetPawns(board)
        board.setPawnLocation(pr0, board.entryCell("blue"))
        board.setPawnLocation(pr1, MainRingCell(board.entryCell("blue").num + 20))
        dice = [2, 3]
        self.assertFalse(board.isSafetyCell(MainRingCell(board.entryCell("blue").num + 20)))
        self.assertEqual(mplayer.doMove(board, dice), [
            EnterPiece(pb0),
            MoveMain(pb0, 20, board.entryCell("blue").num),
            MoveMain(pb0, 20, board.entryCell("blue").num + 20),
        ])

        # pb0 moves home, then pb1 moves and uses the bonus
        self._resetPawns(board)
        board.setPawnLocation(pb0, HomeRowCell(4, pb0.color))
        board.setPawnLocation(pb1, MainRingCell(60))
        dice = [4, 3]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveHome(pb0, 3, 4),
            MoveMain(pb1, 4, 60),
            MoveMain(pb1, 10, 64)
        ])

        # pb0 and pb1 can both move home from the same starting cell
        # (doesn't count as moving a blockade)
        self._resetPawns(board)
        board.setPawnLocation(pb0, HomeRowCell(4, pb0.color))
        board.setPawnLocation(pb1, HomeRowCell(4, pb1.color))
        dice = [4, 4, 3, 3]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveHome(pb0, 3, 4),
            MoveHome(pb1, 3, 4)
        ])

        # pr0 and pr1 form a blockade at cell 1
        # pb1 can move forward 1 to form a blockade with pb0
        # no blue pawns can move because pb0 and pb1 can't move past the blockade
        # and pb2 cannot join pb0 and pb1 on cell 0
        self._resetPawns(board)
        board.setPawnLocation(pb0, MainRingCell(0))
        board.setPawnLocation(pb1, MainRingCell(67))
        board.setPawnLocation(pb2, MainRingCell(66))
        board.setPawnLocation(pr0, MainRingCell(1))
        board.setPawnLocation(pr1, MainRingCell(1))
        dice = [1, 2]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb1, 1, 67)
        ])

        # pb0 can move 3 into MainRingCell(4)
        # no other blue pawns can move because pb1 would form a blockade again
        # with pb0 if it moved 3, and pr0/pr1 form a blockade in front of pb0
        self._resetPawns(board)
        board.setPawnLocation(pb0, MainRingCell(1))
        board.setPawnLocation(pb1, MainRingCell(1))
        board.setPawnLocation(pr0, MainRingCell(5))
        board.setPawnLocation(pr1, MainRingCell(5))
        dice = [3, 3, 4, 4]
        self.assertEqual(mplayer.doMove(board, dice), [
            MoveMain(pb0, 3, 1)
        ])
