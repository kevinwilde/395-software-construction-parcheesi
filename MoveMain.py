from typing import List, Tuple

from Board import Board
from IMove import IMove
from MainRingCell import MainRingCell
from MoveExecutor import MoveExecutor
from Pawn import Pawn

class MoveMain(IMove):
    def __init__(self, pawn: Pawn, distance: int, start: int) -> None:
        """
        Represents a move that starts on the main ring (but does not have to
        end up there).
        """
        self.pawn = pawn
        self.distance = distance
        self.start = start

    def __eq__(self, other) -> bool:
        return (self.pawn == other.pawn and
                self.distance == other.distance and
                self.start == other.start)

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __repr__(self) -> str:
        return ("<MoveMain distance=" + str(self.distance) +
                " start=" + str(self.start) + " pawn=" + str(self.pawn) + ">")

    def execute(self, board: Board, me: MoveExecutor) -> Tuple[Board, int]:
        assert self.pawn in board.pawn_locations
        assert board.getPawnLocation(self.pawn) == MainRingCell(self.start)
        board.advancePawn(self.pawn, self.distance)
        if me.bop(board, self.pawn):
            return board, me.BOP_BONUS
        if board.isInHome(self.pawn):
            return board, me.HOME_BONUS
        return board, 0

    def consumeDice(self, dice: List[int]) -> List[int]:
        assert sum(dice) > 0
        dice.pop(dice.index(self.distance))
        return dice
