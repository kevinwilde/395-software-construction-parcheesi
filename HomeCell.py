from Cell import Cell

class HomeCell(Cell):
    def __init__(self, color: str) -> None:
        self.color = color

    def __eq__(self, other) -> bool:
        if type(other) is not HomeCell:
            return False
        return self.color == other.color

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __repr__(self) -> str:
        return "<HomeCell " + self.color + ">"

    def next(self, board, color: str) -> Cell:
        assert self.color == color
        return None

    def distanceFromNest(self, board, color: str) -> int:
        assert self.color == color
        return board.NUM_CELLS_IN_MAIN_RING + board.NUM_CELLS_IN_HOME_ROW
