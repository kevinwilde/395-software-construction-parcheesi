import threading
from typing import List

from Board import Board
from HView import HView
from IMove import IMove
from Player import Player

class HPlayer(Player):
    def __init__(self, window) -> None:
        super().__init__()
        self.cur_move = None # type: IMove
        self.gui = HView(window, self.setCurMove)
        threading.Thread(target=self.gui.mainloop)
        self.gui.message('Waiting for turn...')

    def setCurMove(self, move: IMove) -> None:
        self.cur_move = move

    def startGame(self, color: str) -> str:
        super().startGame(color)
        return 'HPlayer'

    def _chooseMove(self, board: Board, dice: List[int], orig_brd: Board,
                    moves: List[IMove]) -> IMove:
        """
        PRECONDITION: There must be a legal move available.
        """
        self.gui.message('Your turn, player {}!\nDice: {}'.format(self.color, dice))
        pawns = board.getPawns(self.color)
        legal_moves = list(self.re.generateLegalMoves(pawns, board, dice, orig_brd))

        if len(legal_moves) == 0:
            raise Exception("Precondition Violation: called _chooseMove when no legal moves available")

        self.gui.doMove(board, legal_moves)
        # User will choose a move which will modify self.cur_move
        self.gui.wait_variable(self.gui.doneVar)
        self.gui.message('Waiting for turn...')
        return self.cur_move

    def doublesPenalty(self) -> None:
        self.gui.message('Doubles penalty!')
