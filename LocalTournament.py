import tkinter as tk

from Board import Board
from HPlayer import HPlayer
from MPlayerFirstPawn import MPlayerFirstPawn
from MPlayerHeuristic import MPlayerHeuristic
from MPlayerLastPawn import MPlayerLastPawn
from Tournament import Tournament

class LocalTournament(Tournament):
    def __init__(self, num_humans: int=0) -> None:
        super().__init__()
        assert 0 <= num_humans <= Board.NUM_PLAYERS
        self.num_humans = num_humans
        if self.num_humans > 0:
            root = tk.Tk()
            root.withdraw()

    def registerPlayers(self) -> None:
        # Register Human players
        for _ in range(self.num_humans):
            assert self.admin.register(HPlayer(tk.Toplevel()))

        # Register Machine players
        num_machines = self.admin.game.board.NUM_PLAYERS - self.num_humans
        for i in range(num_machines):
            if i == 0:
                assert self.admin.register(MPlayerHeuristic())
            else:
                assert self.admin.register(MPlayerFirstPawn())
