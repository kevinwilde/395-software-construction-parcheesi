from typing import List

from Board import Board
from IMove import IMove

class IPlayer:
    def __init__(self) -> None:
        self.color = None # type: str
        raise NotImplementedError("You can't initialize an interface.")

    def startGame(self, color: str) -> str:
        """
        Inform the player that a game has started and what color the player is.
        """
        raise NotImplementedError("This is an interface method.")

    def doMove(self, board: Board, dice: List[int]) -> List[IMove]:
        """
        Ask the player what moves they want to make.
        """
        raise NotImplementedError("This is an interface method.")

    def doublesPenalty(self) -> None:
        """
        Inform the player that they have suffered a doubles penalty.
        """
        raise NotImplementedError("This is an interface method.")
