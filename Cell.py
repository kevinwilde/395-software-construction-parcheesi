class Cell:
    def __init__(self) -> None:
        raise NotImplementedError("You can't initialize an abstract base class.")

    def __eq__(self, other) -> bool:
        raise NotImplementedError("This is an abstract base class method.")

    def __neq__(self, other) -> bool:
        raise NotImplementedError("This is an abstract base class method.")

    def next(self, board, color: str) -> 'Cell':
        raise NotImplementedError("This is an abstract base class method.")

    def distanceFromNest(self, board, color: str) -> int:
        raise NotImplementedError("This is an abstract base class method.")
