from Cell import Cell
from HomeCell import HomeCell

class HomeRowCell(Cell):
    def __init__(self, num: int, color: str) -> None:
        assert type(num) is int
        assert type(color) is str
        self.num = num
        self.color = color

    def __eq__(self, other) -> bool:
        if type(other) is not HomeRowCell:
            return False
        return (self.num == other.num and
                self.color == other.color)

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __repr__(self) -> str:
        return "<HomeRowCell " + str(self.num) + " " + self.color + ">"

    def next(self, board, color: str) -> Cell:
        assert self.color == color
        if self.num+1 == board.NUM_CELLS_IN_HOME_ROW:
            return HomeCell(color)
        else:
            return HomeRowCell(self.num+1, color)

    def distanceFromNest(self, board, color: str) -> int:
        assert self.color == color
        return board.NUM_CELLS_IN_MAIN_RING + self.num
