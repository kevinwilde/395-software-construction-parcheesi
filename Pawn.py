import Board

class Pawn:
    def __init__(self, id: int, color: str) -> None:
        assert id in range(Board.Board.NUM_PAWNS_PER_PLAYER)
        assert color in Board.Board.COLORS
        self.id = id
        self.color = color

    def __eq__(self, other) -> bool:
        return self.id == other.id and self.color == other.color

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.id, self.color))

    def __repr__(self) -> str:
        return "<Pawn id=" + str(self.id) + " color=" + self.color + ">"
