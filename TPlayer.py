from typing import List

from Board import Board
from IMove import IMove
from IPlayer import IPlayer

class TPlayer(IPlayer):
    def __init__(self) -> None:
        self.color = None # type: str

    def startGame(self, color: str) -> str:
        """
        Inform the player that a game has started and what color the player is.
        """
        self.color = color
        return 'test'

    def doMove(self, board: Board, dice: List[int]) -> List[IMove]:
        """
        Ask the player what moves they want to make.
        """
        return []

    def doublesPenalty(self) -> None:
        """
        Inform the player that they have suffered a doubles penalty.
        """
        pass
