from typing import List, Union

from Board import Board
from IGame import IGame
from IPlayer import IPlayer
from Pawn import Pawn
from SPlayer import SPlayer

class Game(IGame):
    def __init__(self, board: Board) -> None:
        self.board = board # type: Board
        self.splayers = [] # type: List[SPlayer]
        self.state = Registration() # type: Union[Registration, InProgress]

    def register(self, player: IPlayer) -> bool:
        """
        Add a player to the game.
        """
        return self.state.register(self, player)

    def start(self) -> None:
        """
        Start a game.
        """
        self.state.start(self)

class Registration:
    def register(self, game: Game, player: IPlayer) -> bool:
        if len(game.splayers) >= game.board.NUM_PLAYERS:
            return False
        color = game.board.COLORS[len(game.splayers)]
        game.splayers.append(SPlayer(player, color))
        return True

    def start(self, game: Game) -> None:
        game.state = InProgress()
        for splayer in game.splayers:
            _ = splayer.startGame(splayer.color)
            for i in range(game.board.NUM_PAWNS_PER_PLAYER):
                game.board.addPawn(Pawn(i, splayer.color))

class InProgress:
    def register(self, game: Game, player: IPlayer) -> bool:
        raise ValueError("Cannot register from state InProgress")

    def start(self, game: Game) -> None:
        raise ValueError("Cannot start from state InProgress")
