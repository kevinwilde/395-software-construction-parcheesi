Josh Shi <joshshi@u.northwestern.edu>

Kevin Wilde <kevinwilde2018@u.northwestern.edu>



# Running Tests

## Unit Tests
```
python3 -m unittest discover
```

For unit test coverage report

```
coverage run -m unittest discover && coverage report
```

Or

```
coverage run -m unittest discover && coverage html
```

and then open htmlcov/index.html in a browser.


## Integration Tests
```
python3 -m unittest test/integration.py
```

## Check Rules Against Robby's Rules
```
python3 -m unittest test/compareToRobby.py
```


# MyPy
We added type annotations so that we can statically check for type errors
using [mypy](http://mypy-lang.org/).

```
mypy *.py
```
