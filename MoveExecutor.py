from copy import deepcopy
from typing import Tuple

from Board import Board
from IMove import IMove
from NestCell import NestCell
from Pawn import Pawn

class MoveExecutor:
    BOP_BONUS = 20
    HOME_BONUS = 10

    def executeMove(self, board: Board, move: IMove) -> Tuple[Board, int]:
        """
        Returns a new board and a possible bonus die roll
        PRECONDITION: Only called on legal moves.
        """
        assert isinstance(move, IMove)
        return move.execute(deepcopy(board), self)

    def bop(self, board: Board, pawn: Pawn) -> bool:
        """
        Bop the occupant of the board, if any, at `pawn`'s position
        """
        for p in board.pawn_locations:
            if (board.getPawnLocation(p) == board.getPawnLocation(pawn)
                and p.color != pawn.color):
                board.setPawnLocation(p, NestCell(p.color))
                return True
        return False

    def doublesPenalty(self, board: Board, color: str) -> None:
        p = board.farthestPawn(color)
        if p:
            board.setPawnLocation(p, NestCell(p.color))
