import subprocess
import tkinter as tk
from PIL import Image, ImageTk # type: ignore
from typing import Dict, List

import ParcheesiXML as PXML
from Board import Board
from IMove import IMove
from MoveExecutor import MoveExecutor

class HView(tk.Frame):
    def __init__(self, window, chooseMoveFnc) -> None:
        self.master = window
        self.master.wm_title("Parcheesi")
        super().__init__(self.master)
        self.chooseMoveFnc = chooseMoveFnc
        self.images = {} # type: Dict[str, ImageTk.PhotoImage]
        self.board = None # type: Board
        self.moves = [] # type: List[IMove]
        self.move_idx = 0
        # Need doneVar so that we can use wait_variable to synchronously wait
        # for human to choose move
        self.doneVar = tk.BooleanVar()
        self.me = MoveExecutor()
        self.pack()
        self.create_widgets()

    def create_widgets(self) -> None:
        # Displaying messages to user
        self.msgtext = tk.StringVar()
        self.msg = tk.Label(self, textvariable=self.msgtext)
        self.msg.pack()

        self.container_frame = tk.Frame(self)

        # Frame for current board and next board
        boards_frame = tk.Frame(self.container_frame)
        boards_frame.pack()

        # Frame for current board
        cur_brd_frame = tk.Frame(boards_frame)
        cur_brd_frame.pack(side=tk.LEFT, anchor=tk.SW)

        # Label for board
        cur_brd_label_text = tk.StringVar()
        cur_brd_label_text.set("Current Board")
        self.cur_brd_label = tk.Label(cur_brd_frame, textvariable=cur_brd_label_text)
        self.cur_brd_label.pack()

        # Image to show board
        self.cur_brd_img = tk.Label(cur_brd_frame)
        self.cur_brd_img.pack()

        # Frame for next board
        next_brd_frame = tk.Frame(boards_frame)
        next_brd_frame.pack(side=tk.LEFT, anchor=tk.SW)

        # Label for board
        self.next_brd_label_text = tk.StringVar()
        self.next_brd_label = tk.Label(next_brd_frame, textvariable=self.next_brd_label_text)
        self.next_brd_label.pack()

        # Image to show board
        self.next_brd_img = tk.Label(next_brd_frame)
        self.next_brd_img.pack()

        # Frame for prev and next buttons
        prev_next_frame = tk.Frame(self.container_frame)
        prev_next_frame.pack()
        # Button to see prev board
        self.prev_btn = tk.Button(prev_next_frame, text="Prev", command=self.prev)
        self.prev_btn.pack(side=tk.LEFT)
        # Button to see next board
        self.next_btn = tk.Button(prev_next_frame, text="Next", command=self.next)
        self.next_btn.pack(side=tk.LEFT)

        # Button to choose a move
        self.choose_btn = tk.Button(self.container_frame, text="Choose this move", command=self.selectMove)
        self.choose_btn.pack()

    def doMove(self, board: Board, legal_moves: List[IMove]) -> None:
        self.cur_brd = board
        self.moves = legal_moves
        self.container_frame.pack()
        self.showBoard(self.cur_brd, self.cur_brd_img)
        self.move_idx = 0
        self.showMove()

    def selectMove(self) -> None:
        move = self.moves[self.move_idx]
        self.chooseMoveFnc(move)
        self.doneVar.set(True)
        self.reset()

    def reset(self) -> None:
        self.container_frame.pack_forget()
        self.move_idx = 0
        self.moves = []
        self.doneVar.set(False)

    def message(self, msg: str) -> None:
        self.msgtext.set(msg)
        self.msg.pack()

    def next(self) -> None:
        self.move_idx = (self.move_idx + 1) % len(self.moves)
        self.showMove()

    def prev(self) -> None:
        self.move_idx = (self.move_idx - 1) % len(self.moves)
        self.showMove()

    def showMove(self) -> None:
        move = self.moves[self.move_idx]
        brd, _ = self.me.executeMove(self.cur_brd, move)
        self.showBoard(brd, self.next_brd_img)
        move_text = 'Option {}/{}\n{}'.format(self.move_idx+1, len(self.moves), move)
        self.next_brd_label_text.set(move_text)
        self.next_brd_label.pack()

    def showBoard(self, board: Board, label: tk.Label) -> None:
        board_file = 'tmp.xml'
        with open(board_file, 'w+') as f:
            f.write(PXML.tostring(PXML.BoardToXML(board)))
        img = self.makeImage(board_file)
        label.configure(image=img)
        label.pack()
        # Keep reference to img so it doesn't get garbage collected
        # because then it won't show up in tkinter GUI
        self.images[str(label)] = img

    def makeImage(self, board_file: str) -> ImageTk.PhotoImage:
        img_file = 'tmp.png'
        cmd = ['racket', 'robby/show-board.rkt', '-p', img_file, board_file]
        subprocess.run(cmd)
        img = Image.open(img_file).convert('RGB')
        return ImageTk.PhotoImage(img)
