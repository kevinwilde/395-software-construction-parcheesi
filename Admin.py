import random
from copy import deepcopy
from typing import List, Union

from Board import Board
from Game import Game
from IGame import IGame
from IPlayer import IPlayer
from MoveExecutor import MoveExecutor
from RuleEnforcer import RuleEnforcer
from SPlayer import SPlayer

class Admin:
    DIE_SIZE = 6

    def __init__(self) -> None:
        self.me = MoveExecutor()
        self.re = RuleEnforcer()
        self.game = None # type: IGame
        self.state = GameOver() # type: Union[GameOver, Registration, InProgress]

    def newGame(self) -> None:
        self.state.newGame(self)

    def register(self, player: IPlayer) -> bool:
        return self.state.register(self, player)

    def ready(self) -> None:
        self.state.ready(self)

    def loop(self) -> None:
        self.state.loop(self)


class GameOver:
    """
    GameOver state for Admin
    """
    def newGame(self, admin: Admin) -> None:
        admin.game = Game(Board())
        admin.state = Registration()

    def register(self, admin: Admin, player: IPlayer) -> bool:
        raise ValueError("Cannot register from state GameOver")

    def ready(self, admin: Admin) -> None:
        raise ValueError("Cannot start from state GameOver")

    def loop(self, admin: Admin) -> None:
        raise ValueError("Cannot loop from state GameOver")


class Registration:
    """
    Registration state for Admin
    """
    def newGame(self, admin: Admin) -> None:
        raise ValueError("Cannot newGame from state Registration")

    def register(self, admin: Admin, player: IPlayer) -> bool:
        return admin.game.register(player)

    def ready(self, admin: Admin) -> None:
        admin.state = InProgress()
        admin.game.start()

    def loop(self, admin: Admin) -> None:
        raise ValueError("Cannot loop from state Registration")


class InProgress:
    """
    InProgress state for Admin
    """
    def newGame(self, admin: Admin) -> None:
        raise ValueError("Cannot newGame from state InProgress")

    def register(self, admin: Admin, player: IPlayer) -> bool:
        raise ValueError("Cannot register from state InProgress")

    def ready(self, admin: Admin) -> None:
        raise ValueError("Cannot start from state InProgress")

    def loop(self, admin: Admin) -> None:
        i = 0
        while not admin.re.isGameOver(admin.game.board):
            cur_splayer = admin.game.splayers[i]
            self._giveTurn(admin, cur_splayer)
            i = (i + 1) % len(admin.game.splayers)
        admin.state = GameOver()

    def _giveTurn(self, admin: Admin, splayer: SPlayer) -> None:
        doubles_in_a_row = 0
        turn_over = False
        while not turn_over and not admin.re.isGameOver(admin.game.board):
            dice = self._rollDice(admin)
            pawns_in_nest = [admin.game.board.isInNest(p)
                             for p in admin.game.board.getPawns(splayer.color)]

            if dice[0] == dice[1] and not any(pawns_in_nest):
                dice += [admin.DIE_SIZE + 1 - dice[0], admin.DIE_SIZE + 1 - dice[0]]
                doubles_in_a_row += 1

                if doubles_in_a_row > admin.re.MAX_DOUBLES:
                    # penalty and break
                    admin.me.doublesPenalty(admin.game.board, splayer.color)
                    splayer.doublesPenalty()
                    return
            else:
                turn_over = True

            moves = splayer.doMove(deepcopy(admin.game.board), deepcopy(dice))
            if admin.re.isLegalTurn(splayer.color, admin.game.board, dice, moves):
                for move in moves:
                    admin.game.board, _ = admin.me.executeMove(admin.game.board, move)

    def _rollDice(self, admin: Admin) -> List[int]:
        return [random.randint(1, admin.DIE_SIZE),
                random.randint(1, admin.DIE_SIZE)]
