Josh Shi, Kevin Wilde

Stories (use cases):

1. Registering players
Main creates and registers players with the admin.
Once main has registered the players, it requests the admin to play a round of the game.

2. Playing a turn
The admin plays rounds until one player wins.
At the beginning of the game, one player is randomly selected to go first. Turns are then taken in counter-clockwise order.
During each turn, the admin rolls two dice for the player. The turn then follows the following order of operations:

	1. If one of the dice has a five or if both dices sum to five and the player has at least one pawn in the start area, the player is given the option to move a pawn out of the start area. If all of the player's pawns are in the start area, the admin automatically moves a pawn out. Moving a pawn out consumes the five and that "mini-turn" is over.
	2. When the player has at least one pawn out of the start area and the player did not roll doubles, the player moves a piece by the number indicated by the pips. This is done one die at a time, and the player can choose which pawn is affected by each die independently.
	3. If the player rolls doubles, the admin checks if that player has already rolled doubles twice in a row, in which case the pawn that is furthest along is moved back to the starting circle, and the player's turn is forfeited. Otherwise, if all the player's pieces are out of the starting area, the player moves by the tops and bottoms of the dice, where each top and bottom is an independent "mini-turn" (e.g. four "mini-turns"). The player then takes another turn, regardless of how many of its pawns are on the board.
	4. If the player has at least one pawn which is able to move, he must move a pawn. For each mini-move, the player is presented with all valid moves and can choose which pawn to move and how many spaces to move it. A pawn may not be moved if there is a "blockade" in between it and its finishing position. A pawn may also not be moved if it is in its home row and the roll is greater than the number of spaces between it and home. A pawn may also not be moved if there are already two pawns in the destination cell. A pawn may also not be moved if there is one pawn of another color in the destination cell and the destination cell is a "safety square."
	5. If the cell a pawn lands on is occupied by another pawn of a different color, the previous occupant is "bopped" and sent back to the start area. The moving player may now move any single pawn by 20 spaces.
	6. After the pawn has circumnavigated the main ring, the pawn enters the "home row." If a pawn is moved into the player's "home," that player gets an additional 10-space mini-move

3. End of game
The game ends when one player has all its pawns in the home cell. When this occurs, the admin informs each player object about the player's results

Interfaces:
Model consists of Admin, Game, and Player:

Admin
  class Admin
  class SPlayer

  class RuleEnforcer
  class MoveExecutor

GAME
  class IGame
  class Game(IGame)

  class Board
  class Pawn

  class IMove
  class EnterPiece(IMove)
  class MoveMain(IMove)
  class MoveHome(IMove)

PLAYER
  # interfaces
  class IPlayer

  class Player(IPlayer)
  class MPlayer(Player)
  class HPlayer(Player)

-------------------------------------------

Playing Parcheesi Automatically
Prioritize moving pawns from the starting area into the main ring over moving pawns which are already in the main ring. If no pawns are able to be legally moved into the main ring, attempt to move a pawn which is already out on the main ring.
Prioritize moving the pawn closest to the starting area over the pawn farthest ahead. If this pawn can't legally be moved, attempt to move the pawn which is the next closest to the starting area.
