from typing import List, Tuple

from Board import Board
from IMove import IMove
from HomeRowCell import HomeRowCell
from MoveExecutor import MoveExecutor
from Pawn import Pawn

class MoveHome(IMove):
    def __init__(self, pawn: Pawn, distance: int, start: int) -> None:
        """
        Represents a move that starts on one of the home rows.
        """
        self.pawn = pawn
        self.distance = distance
        self.start = start

    def __eq__(self, other) -> bool:
        return (self.pawn == other.pawn and
                self.distance == other.distance and
                self.start == other.start)

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __repr__(self) -> str:
        return ("<MoveHome distance=" + str(self.distance) +
                " start=" + str(self.start) + " pawn=" + str(self.pawn) + ">")

    def execute(self, board: Board, me: MoveExecutor) -> Tuple[Board, int]:
        assert board.getPawnLocation(self.pawn) == HomeRowCell(self.start, self.pawn.color)
        board.advancePawn(self.pawn, self.distance)
        if board.isInHome(self.pawn):
            return board, me.HOME_BONUS
        return board, 0

    def consumeDice(self, dice: List[int]) -> List[int]:
        assert sum(dice) > 0
        dice.pop(dice.index(self.distance))
        return dice
