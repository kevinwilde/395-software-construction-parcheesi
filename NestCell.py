from Cell import Cell

class NestCell(Cell):
    def __init__(self, color: str) -> None:
        self.color = color

    def __eq__(self, other) -> bool:
        if type(other) is not NestCell:
            return False
        return self.color == other.color

    def __ne__(self, other) -> bool:
        return not self.__eq__(other)

    def __repr__(self) -> str:
        return "<NestCell " + self.color + ">"

    def next(self, board, color: str) -> Cell:
        assert self.color == color
        return board.entryCell(color)

    def distanceFromNest(self, board, color: str) -> int:
        assert self.color == color
        return 0
