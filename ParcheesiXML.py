"""
XML Library for Parcheesi
"""

import xml.etree.ElementTree as ET
from typing import List, Tuple, Union

from Board import Board
from EnterPiece import EnterPiece
from HomeCell import HomeCell
from HomeRowCell import HomeRowCell
from IMove import IMove
from MainRingCell import MainRingCell
from MoveHome import MoveHome
from MoveMain import MoveMain
from NestCell import NestCell
from Pawn import Pawn

def recvXML(socket) -> ET.Element:
    buf_size = 1024
    data = ''
    while True:
        data += socket.recv(buf_size).decode('utf-8')
        if '\n' in data:
            return fromstring(data)

def tostring(elt: ET.Element) -> str:
    return ET.tostring(elt, short_empty_elements=False).decode('utf-8')

def fromstring(s: str) -> ET.Element:
    return ET.fromstring(s)

def XMLIsStartGame(xml: ET.Element) -> bool:
    return xml.tag == 'start-game'

def XMLIsDoMove(xml: ET.Element) -> bool:
    return xml.tag == 'do-move'

def XMLIsDoublesPenalty(xml: ET.Element) -> bool:
    return xml.tag == 'doubles-penalty'

def VoidToXML() -> ET.Element:
    return ET.Element('void')

def StartGameToXML(color: str) -> ET.Element:
    xml_startgame = ET.Element('start-game')
    xml_startgame.text = color
    return xml_startgame

def XMLToStartGame(xml: ET.Element) -> str:
    assert xml.tag == 'start-game'
    return xml.text

def NameToXML(name: str) -> ET.Element:
    xml_name = ET.Element('name')
    xml_name.text = name
    return xml_name

def XMLToName(xml: ET.Element) -> str:
    assert xml.tag == 'name'
    return xml.text

def DoMoveToXML(board: Board, dice: List[int]) -> ET.Element:
    xml_domove = ET.Element('do-move')
    xml_domove.append(BoardToXML(board))
    xml_domove.append(DiceToXML(dice))
    return xml_domove

def XMLToDoMove(xml: ET.Element) -> Tuple[Board, List[int]]:
    assert xml.tag == 'do-move'
    xml_board = xml.find('board')
    xml_dice = xml.find('dice')
    return XMLToBoard(xml_board), XMLToDice(xml_dice)

def DoublesPenaltyToXML() -> ET.Element:
    return ET.Element('doubles-penalty')

def BoardToXML(board) -> ET.Element:
    xml_board = ET.Element('board')
    xml_start = ET.Element('start')
    xml_main = ET.Element('main')
    xml_homerows = ET.Element('home-rows')
    xml_home = ET.Element('home')
    xml_board.append(xml_start)
    xml_board.append(xml_main)
    xml_board.append(xml_homerows)
    xml_board.append(xml_home)

    for (p, c) in board.pawn_locations.items():
        if type(c) is NestCell:
            xml_start.append(PawnToXML(p))
        elif type(c) is MainRingCell:
            xml_main.append(PawnLocToXML(p, c.num))
        elif type(c) is HomeRowCell:
            xml_homerows.append(PawnLocToXML(p, c.num))
        elif type(c) is HomeCell:
            xml_home.append(PawnToXML(p))

    return xml_board

def XMLToBoard(xml: ET.Element) -> Board:
    assert xml.tag == 'board'
    b = Board()

    start = xml.find('start')
    for child in start:
        pawn = XMLToPawn(child)
        b.addPawn(pawn)

    main = xml.find('main')
    for child in main:
        pawn, num = XMLToPawnLoc(child)
        b.addPawn(pawn)
        b.setPawnLocation(pawn, MainRingCell(num))

    homerows = xml.find('home-rows')
    for child in homerows:
        pawn, num = XMLToPawnLoc(child)
        b.addPawn(pawn)
        b.setPawnLocation(pawn, HomeRowCell(num, pawn.color))

    home = xml.find('home')
    for child in home:
        pawn = XMLToPawn(child)
        b.addPawn(pawn)
        b.setPawnLocation(pawn, HomeCell(pawn.color))

    return b

def DiceToXML(dice: List[int]) -> ET.Element:
    xml_dice = ET.Element('dice')
    for die in dice:
        xml_die = ET.Element('die')
        xml_die.text = str(die)
        xml_dice.append(xml_die)
    return xml_dice

def XMLToDice(xml: ET.Element) -> List[int]:
    assert xml.tag == 'dice'
    dice = []
    for die in xml:
        dice.append(int(die.text))
    return dice

def MovesToXML(moves: List[IMove]) -> ET.Element:
    xml_moves = ET.Element('moves')
    for move in moves:
        xml_moves.append(MoveToXML(move))
    return xml_moves

def XMLToMoves(xml: ET.Element) -> List[IMove]:
    assert xml.tag == 'moves'
    moves = []
    for move in xml:
        moves.append(XMLToMove(move))
    return moves

def MoveToXML(move) -> ET.Element:
    if type(move) is EnterPiece:
        xml_move = ET.Element('enter-piece')
        xml_move.append(PawnToXML(move.pawn))
    else:
        if type(move) is MoveMain:
            xml_move = ET.Element('move-piece-main')
        elif type(move) is MoveHome:
            xml_move = ET.Element('move-piece-home')
        xml_move.append(PawnToXML(move.pawn))
        xml_start = ET.Element('start')
        xml_start.text = str(move.start)
        xml_move.append(xml_start)
        xml_distance = ET.Element('distance')
        xml_distance.text = str(move.distance)
        xml_move.append(xml_distance)
    return xml_move

def XMLToMove(xml: ET.Element) -> IMove:
    move = None # type: IMove
    if xml.tag == 'enter-piece':
        move = EnterPiece(XMLToPawn(xml.find('pawn')))
    else:
        pawn = XMLToPawn(xml.find('pawn'))
        start = int(xml.find('start').text)
        distance = int(xml.find('distance').text)
        if xml.tag == 'move-piece-main':
            move = MoveMain(pawn, distance, start)
        elif xml.tag == 'move-piece-home':
            move = MoveHome(pawn, distance, start)
    return move

def PawnLocToXML(pawn: Pawn, num: int) -> ET.Element:
    """
    Takes a pawn and a cell num and generates a `<piece-loc>` XML element
    """
    xml_pl = ET.Element('piece-loc')
    xml_pawn = PawnToXML(pawn)
    xml_pl.append(xml_pawn)
    xml_loc = ET.Element('loc')
    xml_loc.text = str(num)
    xml_pl.append(xml_loc)
    return xml_pl

def XMLToPawnLoc(xml: ET.Element) -> Tuple[Pawn, int]:
    """
    Takes a `<piece-loc>` XML element and generates a pawn and a cell num
    """
    assert xml.tag == 'piece-loc'
    pawn = XMLToPawn(xml.find('pawn'))
    num = int(xml.find('loc').text)
    return (pawn, num)

def PawnToXML(pawn: Pawn) -> ET.Element:
    xml_pawn = ET.Element('pawn')
    xml_color = ET.Element('color')
    xml_color.text = pawn.color
    xml_pawn.append(xml_color)
    xml_id = ET.Element('id')
    xml_id.text = str(pawn.id)
    xml_pawn.append(xml_id)
    return xml_pawn

def XMLToPawn(xml: ET.Element) -> Pawn:
    assert xml.tag == 'pawn'
    return Pawn(int(xml.find('id').text), xml.find('color').text)
